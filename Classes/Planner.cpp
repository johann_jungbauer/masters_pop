#include "Planner.h"

Planner::Planner()
{

}

bool Planner::getAgentActionSequence()
{
    bool viable = true;

    /// basic variables
    Coord startC = Coord(100,100);
    Coord homeC = Coord(150,550);
    Coord marketC = Coord(150,150);
    Coord diyC = Coord(450,350);

    std::string drillStr = "drill";
    std::string bananaStr = "banana";
    std::string milkStr = "milk";

    Var_Location start = Var_Location(startC);
    Var_Location home = Var_Location(homeC);
    Var_Location market = Var_Location(marketC);
    Var_Location diy = Var_Location(diyC);

    Var_Item drill = Var_Item(drillStr);
    Var_Item banana = Var_Item(bananaStr);
    Var_Item milk = Var_Item(milkStr);

    /// defining the start state
    int start_var_num = 7;
    vector<Predicate* > start_state;
    //start_state.push_back(new Pred_At(&home));
    start_state.push_back(new Pred_At(&start));
    start_state.push_back(new Pred_Sells(&market, &banana));
    start_state.push_back(new Pred_Sells(&diy, &drill));
    start_state.push_back(new Pred_Sells(&market, &milk));

    /// defining the goal state
    int goal_var_num = 4;
    vector<Predicate* > goal_state;
    goal_state.push_back(new Pred_Have(&drill));
    goal_state.push_back(new Pred_Have(&banana));
    goal_state.push_back(new Pred_Have(&milk));
    goal_state.push_back(new Pred_At(&home));

    std::vector<Operator *> operators;
    Plan plan;

    /// calling the POP algorithm to generate a partially ordered plan
    viable = _POP_Algorithm.run_Algorithm(start_state, start_var_num, goal_state, goal_var_num, operators, plan);

    if(viable == true)
    {
        /// linearizing POP
        vector<Pair> ordering;
        convertOrdersToPairs(plan._ordering, ordering);
        vector<int> totalOrder;
        _interpreter.doInterpret(ordering, totalOrder);
        LOG(FILE_LOG(logDEBUG) << print_Total_Order(totalOrder););
    }

    return viable;
}

void Planner::convertOrdersToPairs(vector<Order> &planOrder, vector<Pair> &convOrder)
{
    for(unsigned int i = 0; i < planOrder.size(); i++)
    {
        Pair p = Pair(planOrder[i].get_Preceeding_Step_Index(), planOrder[i].get_Following_Step_Index());
        convOrder.push_back(p);
    }
}

string Planner::print_Total_Order(vector<int> &total_order)
{
    Common comm;
    string out = "\n\n___Total Order___";
    for(unsigned int i = 0; i < total_order.size(); i++)
    {
        out.append("\n\t");
        out.append(comm.intToString(total_order[i]));
    }

    return out;
}
