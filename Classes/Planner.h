#ifndef PLANNER_H
#define PLANNER_H

#include <vector>

#include "Common/Common.h"
#include "Common/log.h"

#include "Planner/POP_Algorithm.h"
#include "Planner/Interpret.h"
#include "Common/Pair.h"

using namespace std;

class Planner
{
    public:
        Planner();

        bool getAgentActionSequence();

        void convertOrdersToPairs(vector<Order> &planOrder, vector<Pair> &convOrder);
        string print_Total_Order(vector<int> &total_order);

    private:
        POP_Algorithm _POP_Algorithm;
        Interpret _interpreter;


};
#endif


