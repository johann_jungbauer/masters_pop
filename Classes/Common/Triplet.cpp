#include "Triplet.h"

/// Class Constructor
Triplet::Triplet (int x ,int y, int z):
        _x(x),
        _y(y),
        _z(z)
{}

/// Class Constructor
Triplet::Triplet ():
        _x(0),
        _y(0),
        _z(0)
{}

/// Get function for X.
int Triplet::getX()
{
	return _x;
}

/// Set function for X.
void Triplet::setX(int x)
{
	_x = x;
}

/// Get function for Y.
int Triplet::getY()
{
	return _y;
}

/// Set function for Y.
void Triplet::setY(int y)
{
	_y = y;
}

/// Get function for Z.
int Triplet::getZ()
{
	return _z;
}

/// Set function for Y.
void Triplet::setZ(int z)
{
	_z = z;
}

/// equality operator
bool Triplet::operator==(const Triplet &other) const
{
    bool equal = false;

    if((this->_x == other._x) && (this->_y == other._y) && (this->_z == other._z))
    {
        equal = true;
    }

    return equal;
}

bool Triplet::operator <(const Triplet &rhs)const
{
    return (this->_z < rhs._z);
}

bool Triplet::operator!=(const Triplet &other) const
{
    return !(*this == other);
}

std::string Triplet::print()
{
    Common common;
    std::string out = "[";
    out.append(common.intToString(_x));
    out.append(",");
    out.append(common.intToString(_y));
    out.append(",");
    out.append(common.intToString(_z));
    out.append("]");
    return out;
}

