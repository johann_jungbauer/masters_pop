#ifndef TRIPLET_H
#define TRIPLET_H

#include <string>

#include "Common.h"

///
class Triplet {
	public:
        Triplet (int x ,int y, int z);
        Triplet ();

        int getX();
        void setX(int x);
        int getY();
        void setY(int y);
        int getZ();
        void setZ(int z);

        bool operator==(const Triplet &other)const;
        bool operator!=(const Triplet &other)const;
        bool operator <(const Triplet &rhs)const;

        std::string print();

    private:
        int _x;
        int _y;
        int _z;
};

#endif

