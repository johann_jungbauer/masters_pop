#include "Pair.h"

/// Class Constructor
Pair::Pair (int x ,int y):
        _x(x),
        _y(y)
{}

/// Class Constructor
Pair::Pair ():
        _x(-1),
        _y(-1)
{
    //Pair ( -1 , -1 );
}

/// Get function for X.
int Pair::getX()
{
	return _x;
}

/// Set function for X.
void Pair::setX(int x)
{
	_x = x;
}

/// Get function for Y.
int Pair::getY()
{
	return _y;
}

/// Set function for Y.
void Pair::setY(int y)
{
	_y = y;
}

/// equality operator
bool Pair::operator==(const Pair &other) const
{
    bool equal = false;

    if((this->_x == other._x) && (this->_y == other._y))
    {
        equal = true;
    }

    return equal;
}

bool Pair::operator!=(const Pair &other) const
{
    return !(*this == other);
}

std::string Pair::print()
{
    Common common;
    std::string out = "[";
    out.append(common.intToString(_x));
    out.append(",");
    out.append(common.intToString(_y));
    out.append("]");
    return out;
}
