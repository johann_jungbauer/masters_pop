#ifndef PAIR_H
#define PAIR_H

#include <string>

#include "Common.h"

class Pair {
	public:
        Pair (int x ,int y);
        Pair ();

        int getX();
        void setX(int x);
        int getY();
        void setY(int y);

        bool operator==(const Pair &other)const;
        bool operator!=(const Pair &other)const;

        std::string print();

    private:
        int _x;
        int _y;
};

#endif
