#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <sstream>
#include <ctime>
#include <cstdlib>
#include <string>

#include "Coord.h"

#define PI 3.14159265

using namespace std;

class Common
{
    public:
        Common();
        string intToString(int in);
        string boolToString(bool in);
        string floatToString(float in);
        int getRandomInt(int min, int max);
        bool newLineChunks(string &input, string &output);

    private:

};
#endif

