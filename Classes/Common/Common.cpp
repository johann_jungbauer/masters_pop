#include "Common.h"

Common::Common()
{

}

/* Converts an integer to a string
-----------------------------------
Parameters - an integer
Returns - a string
*/
string Common::intToString(int in)
{
    stringstream out;
    out << in;
    return out.str();
}

/* Converts a float to a string
-----------------------------------
Parameters - a float
Returns - a string
*/
string Common::floatToString(float in)
{
    stringstream out;
    out << in;
    return out.str();
}

/* Converts a boolean to a string
-----------------------------------
Parameters - a boolean
Returns - a string
*/
string Common::boolToString(bool in)
{
    string out = "false";

    if(in)
    {
        out = "true";
    }

    return out;
}

/* Produces a random integer between min and max
-----------------------------------
Parameters  - min - minimum value
            - max - maximum value
Returns - an integer
*/
int Common::getRandomInt(int min, int max)
{
    //http://www.daniweb.com/software-development/cpp/threads/1769/c-random-numbers
    srand((unsigned)time(0));
    int random_integer;
    int lowest=min, highest=max;
    int range=(highest-lowest)+1;

    random_integer = lowest+int(range*rand()/(RAND_MAX + 1.0));

    return random_integer;
}

/* Extracts the first part of a string before a newline character
-----------------------------------
Parameters  - input - the whole string to be parsed
            - output - the string extracted
Returns - a boolean, true if a newline char is found
*/
bool Common::newLineChunks(string &input, string &output)
{
    bool found = false;

    size_t find = input.find("\n");

    if(find != string::npos)
    {
        found = true;
        int pos = (int)find;
        output = input.substr(0, pos);
        input.erase(0,pos+1);
    }

    return found;
}
