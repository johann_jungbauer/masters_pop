#include "Link.h"

Link::Link(int effect_step ,int precon_step, int precon_index):
    _effect_Step_Index(effect_step),
    _precon_Step_Index(precon_step),
    _precon_Index(precon_index)
{}

Link::Link():
    _effect_Step_Index(0),
    _precon_Step_Index(0),
    _precon_Index(0)
{}

int Link::get_Effect_Step_Index()
{
    return _effect_Step_Index;
}

void Link::set_Effect_Step_Index(int index)
{
    _effect_Step_Index = index;
}

int Link::get_Precon_Step_Index()
{
    return _precon_Step_Index;
}

void Link::set_Precon_Step_Index(int index)
{
    _precon_Step_Index = index;
}

int Link::get_Precon_Index()
{
    return _precon_Index;
}

void Link::set_Precon_Index(int index)
{
    _precon_Index = index;
}

std::string Link::print()
{
    Common comm;
    std::string out = "{ ";
    out.append(comm.intToString(_effect_Step_Index));
    out.append(" , ");
    out.append(comm.intToString(_precon_Step_Index));
    out.append(" , ");
    out.append(comm.intToString(_precon_Index));
    out.append(" }");

    return out;
}

bool Link::operator==(const Link &other) const
{
    bool equal = false;

    if((this->_effect_Step_Index == other._effect_Step_Index) && (this->_precon_Step_Index == other._precon_Step_Index) && (this->_precon_Index == other._precon_Index))
    {
        equal = true;
    }

    return equal;
}

bool Link::operator!=(const Link &other) const
{
    return !(*this == other);
}

