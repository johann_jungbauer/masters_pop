#ifndef ALGORITHM_NODE_H
#define ALGORITHM_NODE_H

#include <string>
#include <vector>

#include "../Common/Common.h"
#include "Plan.h"
#include "Link.h"
#include "Base_Objects/Operator.h"


/********************************************//**
 * This class is to provide the nodes in the tree structure that will be used
 * by the POP algorithm to deal with deadends.
 ***********************************************/
class Algorithm_Node {
	public:
        Algorithm_Node ();

        std::string print();

        /// Only set at initial node creation
        int _node_ID;
        int _parent_ID;
        int _seed_step_index;
        int _seed_precon_index;
        Operator _seed_operator;

        /// Set at creation & edited just before child creation
        Plan _plan;
        std::vector<Link> _failed_links;

        /// Updated at child creation
        std::vector<int> _children;

        /// Edited if node results in failure
        bool _failed_path;


    private:

};

#endif
