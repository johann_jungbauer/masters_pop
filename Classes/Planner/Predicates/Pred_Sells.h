#ifndef PRED_SELLS_H
#define PRED_SELLS_H

#include "../Base_Objects/Predicate.h"

///

class Pred_Sells : public Predicate {
	public:
        Pred_Sells(Var_Location* loc, Var_Item* item);
        Pred_Sells(Var_Item* item);
        Pred_Sells(Var_Location* loc);

        Coord get_Location();
        std::string get_Item_Name();


    private:

};

#endif
