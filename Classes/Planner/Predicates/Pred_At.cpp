#include "Pred_At.h"

Pred_At::Pred_At(Var_Location* loc) : Predicate(1, AT)
{
    _variable_array[0] = loc;
}

Coord Pred_At::get_Location()
{
    Var_Location *l = dynamic_cast<Var_Location *>(_variable_array[0]);
    return l->get_Location_Coord();
}
