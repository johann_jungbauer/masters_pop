#ifndef PRED_HAVE_H
#define PRED_HAVE_H

#include "../Base_Objects/Predicate.h"

///

class Pred_Have : public Predicate {
	public:
        Pred_Have(Var_Item* item);

        std::string get_Item_Name();

    private:
};

#endif
