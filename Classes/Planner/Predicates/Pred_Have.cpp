#include "Pred_Have.h"

Pred_Have::Pred_Have(Var_Item* item) : Predicate(1, HAVE)
{
    _variable_array[0] = item;
    setPriority(7);
}

std::string Pred_Have::get_Item_Name()
{
    Var_Item *it = dynamic_cast<Var_Item *>(_variable_array[0]);
    return it->get_Item_Name();
    setPriority(7);
}

