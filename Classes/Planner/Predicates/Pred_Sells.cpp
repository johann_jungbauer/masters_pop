#include "Pred_Sells.h"

Pred_Sells::Pred_Sells(Var_Location* loc, Var_Item* item) : Predicate(2, SELLS)
{
    _variable_array[0] = loc;
    _variable_array[1] = item;
}

Pred_Sells::Pred_Sells(Var_Item* item) : Predicate(2, SELLS)
{
    _variable_array[0] = new Var_Location();
    _variable_array[1] = item;
}

Pred_Sells::Pred_Sells(Var_Location* loc) : Predicate(2, SELLS)
{
    _variable_array[0] = loc;
    _variable_array[1] = new Var_Item();
}

Coord Pred_Sells::get_Location()
{
    Var_Location *l = dynamic_cast<Var_Location *>(_variable_array[0]);
    return l->get_Location_Coord();
}

std::string Pred_Sells::get_Item_Name()
{
    Var_Item *it = dynamic_cast<Var_Item *>(_variable_array[1]);
    return it->get_Item_Name();
}
