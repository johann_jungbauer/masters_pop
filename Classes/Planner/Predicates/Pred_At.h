#ifndef PRED_AT_H
#define PRED_AT_H

#include "../Base_Objects/Predicate.h"

///

class Pred_At : public Predicate {
	public:
        Pred_At(Var_Location *loc);

        Coord get_Location();

    private:
};

#endif
