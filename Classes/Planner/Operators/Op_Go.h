#ifndef OP_GO_H
#define OP_GO_H

#include "../Base_Objects/Operator.h"
#include "../Variables/Var_Location.h"
#include "../../Common/Coord.h"

///

class Op_Go : public Operator {
	public:
        Op_Go();
        Op_Go(Coord here, Coord there);
        Op_Go(Coord place, bool effect);

        //~Op_Go();

        bool variable_Rule_Check();

    private:
};

#endif
