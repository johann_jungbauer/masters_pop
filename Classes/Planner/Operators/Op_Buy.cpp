#include "Op_Buy.h"

Op_Buy::Op_Buy() : Operator(BUY, 2, 2, 1, 1, false)
{
    /// set variables
    _var_array[0] = new Var_Location(); /// store
    _var_array[1] = new Var_Item(); /// item

    /// precons
    _precon_array[0] = new Pred_At(dynamic_cast<Var_Location *>(_var_array[0])); /// store
    _precon_array[0]->set_Op_index(0,0);

    _precon_array[1] = new Pred_Sells(dynamic_cast<Var_Location *>(_var_array[0]), dynamic_cast<Var_Item *>(_var_array[1])); /// store & item
    _precon_array[1]->set_Op_index(0,0);
    _precon_array[1]->set_Op_index(1,1);

    /// additions
    _addition_array[0] = new Pred_Have(dynamic_cast<Var_Item *>(_var_array[1])); /// item
    _addition_array[0]->set_Op_index(0,1);

    /// remove
    _remove_array[0] = new Predicate();

    _var_array[0]->set_Defined(false);
    _var_array[1]->set_Defined(false);
}

Op_Buy::Op_Buy(Coord store, std::string item) : Operator(BUY, 2, 2, 1, 1, false)
{
    /// set variables
    _var_array[0] = new Var_Location(store); /// store
    _var_array[1] = new Var_Item(item); /// item

    /// precons
    _precon_array[0] = new Pred_At(dynamic_cast<Var_Location *>(_var_array[0])); /// store
    _precon_array[0]->set_Op_index(0,0);

    _precon_array[1] = new Pred_Sells(dynamic_cast<Var_Location *>(_var_array[0]), dynamic_cast<Var_Item *>(_var_array[1])); /// store & item
    _precon_array[1]->set_Op_index(0,0);
    _precon_array[1]->set_Op_index(1,1);

    /// additions
    _addition_array[0] = new Pred_Have(dynamic_cast<Var_Item *>(_var_array[1])); /// item
    _addition_array[0]->set_Op_index(0,1);

    /// remove
    _remove_array[0] = new Predicate();

    _var_array[0]->set_Defined(true);
    _var_array[1]->set_Defined(true);
}

Op_Buy::Op_Buy(std::string item_name) : Operator(BUY, 2, 2, 1, 1, false)
{
    /// set variables
    _var_array[0] = new Var_Location(); /// store
    _var_array[1] = new Var_Item(item_name); /// item

    /// precons
    _precon_array[0] = new Pred_At(dynamic_cast<Var_Location *>(_var_array[0])); /// store
    _precon_array[0]->set_Op_index(0,0);

    _precon_array[1] = new Pred_Sells(dynamic_cast<Var_Location *>(_var_array[0]), dynamic_cast<Var_Item *>(_var_array[1])); /// store & item
    _precon_array[1]->set_Op_index(0,0);
    _precon_array[1]->set_Op_index(1,1);

    /// additions
    _addition_array[0] = new Pred_Have(dynamic_cast<Var_Item *>(_var_array[1])); /// item
    _addition_array[0]->set_Op_index(0,1);

    /// remove
    _remove_array[0] = new Predicate();

    _var_array[0]->set_Defined(false);
    _var_array[1]->set_Defined(true);
}

Op_Buy::Op_Buy(Coord store) : Operator(BUY, 2, 2, 1, 1, false)
{
    /// set variables
    _var_array[0] = new Var_Location(store); /// store
    _var_array[1] = new Var_Item(); /// item

    /// precons
    _precon_array[0] = new Pred_At(dynamic_cast<Var_Location *>(_var_array[0])); /// store
    _precon_array[0]->set_Op_index(0,0);

    _precon_array[1] = new Pred_Sells(dynamic_cast<Var_Location *>(_var_array[0]), dynamic_cast<Var_Item *>(_var_array[1])); /// store & item
    _precon_array[1]->set_Op_index(0,0);
    _precon_array[1]->set_Op_index(1,1);

    /// additions
    _addition_array[0] = new Pred_Have(dynamic_cast<Var_Item *>(_var_array[1])); /// item
    _addition_array[0]->set_Op_index(0,1);

    /// remove
    _remove_array[0] = new Predicate();

    _var_array[0]->set_Defined(true);
    _var_array[1]->set_Defined(false);
}


/*Op_Buy::~Op_Buy()
{
    delete _precon_array[0];
    delete _precon_array[1];

    delete _effect_array[0];
}*/

