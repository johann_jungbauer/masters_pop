#ifndef OP_BUY_H
#define OP_BUY_H

#include "../Base_Objects/Operator.h"
#include "../../Common/Coord.h"


///

class Op_Buy : public Operator {
	public:
        Op_Buy();
        Op_Buy(Coord store, std::string item);
        Op_Buy(std::string item);
        Op_Buy(Coord store);

        //~Op_Buy();

    private:
};

#endif

