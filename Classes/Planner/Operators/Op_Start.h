#ifndef OP_START_H
#define OP_START_H

#include <iostream>
#include <vector>

#include "../Base_Objects/Operator.h"
#include "../../Common/Coord.h"

///

class Op_Start : public Operator {
	public:
        Op_Start(vector<Predicate* > start_state, int var_num);

        //~Op_Start();

        /// copy constructor
        //Op_Start()

    private:
};

#endif


