#include "Op_Go.h"

/** \brief Basic constructor for the Op_Go class.
 *      Internal variables are undefined pointers.
 */
Op_Go::Op_Go() : Operator(GO, 2, 1, 1, 1, true)
{
    /// set variables
    _var_array[0] = new Var_Location(); /// here
    _var_array[1] = new Var_Location(); /// there

    /// precons
    _precon_array[0] = new Pred_At(dynamic_cast<Var_Location *>(_var_array[0])); /// here
    _precon_array[0]->set_Op_index(0,0); /// should probably make this part of the constructor at some point

    /// addition
    _addition_array[0] = new Pred_At(dynamic_cast<Var_Location *>(_var_array[1])); /// there
    _addition_array[0]->set_Op_index(0,1);

    /// remove
    _remove_array[0] = new Pred_At(dynamic_cast<Var_Location *>(_var_array[0])); /// here
    _remove_array[0]->set_Op_index(0,0);
}

/** \brief Complete constructor for the Op_Go class.
 *      Both internal variables are defined in the parameters.
 *
 * \param here Coord
 * \param there Coord
 *
 */
Op_Go::Op_Go(Coord here, Coord there) : Operator(GO, 2, 1, 1, 1, true)
{
    /// set variables
    _var_array[0] = new Var_Location(here);
    _var_array[1] = new Var_Location(there);

    /// precons
    _precon_array[0] = new Pred_At(dynamic_cast<Var_Location *>(_var_array[0])); /// here
    _precon_array[0]->set_Op_index(0,0); /// should probably make this part of the constructor at some point

    /// addition
    _addition_array[0] = new Pred_At(dynamic_cast<Var_Location *>(_var_array[1])); /// there
    _addition_array[0]->set_Op_index(0,1);

    /// remove
    _remove_array[0] = new Pred_At(dynamic_cast<Var_Location *>(_var_array[0])); /// here
    _remove_array[0]->set_Op_index(0,0);
}

/** \brief Incomplete constructor for Op_Go class.
 *      The first parameter defines the location value.
 *      The second defines if the location is used for "here" or "there".
 *      True for "there". False for "here".
 *
 * \param place Coord
 * \param effect bool
 *
 */
Op_Go::Op_Go(Coord place, bool effect) : Operator(GO, 2, 1, 1, 1, true)
{
    if(effect)
    {
        /// set variables
        _var_array[0] = new Var_Location();
        _var_array[1] = new Var_Location(place); /// there
    }
    else
    {
        /// set variables
        _var_array[0] = new Var_Location(place); /// here
        _var_array[1] = new Var_Location();
    }

    /// precons
    _precon_array[0] = new Pred_At(dynamic_cast<Var_Location *>(_var_array[0])); /// here
    _precon_array[0]->set_Op_index(0,0); /// should probably make this part of the constructor at some point

    /// addition
    _addition_array[0] = new Pred_At(dynamic_cast<Var_Location *>(_var_array[1])); /// there
    _addition_array[0]->set_Op_index(0,1);

    /// remove
    _remove_array[0] = new Pred_At(dynamic_cast<Var_Location *>(_var_array[0])); /// here
    _remove_array[0]->set_Op_index(0,0);
}


/*Op_Go::~Op_Go()
{
    delete _precon_array[0];

    delete _effect_array[0];
    delete _effect_array[1];
}*/

/** \brief Used to check if the object passes the rules applied to to.
 *      The "here" and "there" variables must not be equal.
 *      Returns a false if the "here" and "there" are equal to each other.
 *      Returns a true if the test passes.
 *
 * \return bool
 *
 */
bool Op_Go::variable_Rule_Check()
{
    bool ans = true;

     LOG(FILE_LOG(logDEBUG) << "Op_Go::variable_Rule_Check() --- !!!";);

    /// variables are inconsistent if here and there are equal
    Var_Location *here = dynamic_cast<Var_Location *>(_var_array[0]);
    Var_Location *there = dynamic_cast<Var_Location *>(_var_array[1]);

    if( *here == *there)
    {
        ans = false;
    }

    return ans;
}
