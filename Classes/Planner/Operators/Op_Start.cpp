#include "Op_Start.h"

Op_Start::Op_Start(vector<Predicate* > start_state, int var_num) : Operator(START, var_num, 1, start_state.size(), 1, false)
{
    int var_counter = 0;

    /// precon array
    _precon_array[0] = new Predicate();

    /// addition array
    // TODO (generalize#1#) Move to mediator, need a smarter way to do this.
    for(unsigned int i = 0; i < start_state.size(); i++)
    {
        switch(start_state[i]->get_Predicate_Type())
        {
            case AT:
            {
                Pred_At *at = dynamic_cast<Pred_At *>(start_state[i]);
                Var_Location *l = dynamic_cast<Var_Location *>(at->get_Variable(0));

                _var_array[var_counter] = new Var_Location(l->get_Location_Coord());
                _addition_array[i] = new Pred_At(dynamic_cast<Var_Location *>(_var_array[var_counter]));
                _addition_array[i]->set_Op_index(0,var_counter);
                var_counter++;
            }
            break;

            case HAVE:
            {
                Pred_Have *have = dynamic_cast<Pred_Have *>(start_state[i]);
                Var_Item *it = dynamic_cast<Var_Item *>(have->get_Variable(0));

                _var_array[var_counter] = new Var_Item(it->get_Item_Name());
                _addition_array[i] = new Pred_Have(dynamic_cast<Var_Item *>(_var_array[var_counter]));
                _addition_array[i]->set_Op_index(0,var_counter);
                var_counter++;
            }
            break;

            case SELLS:
            {
                Pred_Sells *sell = dynamic_cast<Pred_Sells *>(start_state[i]);
                Var_Location *l = dynamic_cast<Var_Location *>(sell->get_Variable(0));
                Var_Item *it = dynamic_cast<Var_Item *>(sell->get_Variable(1));

                _var_array[var_counter] = new Var_Location(l->get_Location_Coord());
                var_counter++;
                _var_array[var_counter] = new Var_Item(it->get_Item_Name());
                _addition_array[i] = new Pred_Sells(dynamic_cast<Var_Location *>(_var_array[var_counter-1]), dynamic_cast<Var_Item *>(_var_array[var_counter]));
                _addition_array[i]->set_Op_index(0,var_counter-1);
                _addition_array[i]->set_Op_index(1,var_counter);
                var_counter++;
            }
            break;

            default:
                _addition_array[i] = new Predicate();
        }
    }

    /// remove array
    _remove_array[0] = new Predicate();
}

/*Op_Start::~Op_Start()
{
    delete _precon_array[0];

    for(int i = 0; i < get_Effect_Array_Size(); i++)
    {
        delete _effect_array[i];
    }
}*/
