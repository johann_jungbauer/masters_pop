#include "Var_Item.h"

Var_Item::Var_Item() : Variable(ITEM)
{
    _item_name = "NULL";
    _var_defined = false;
}

Var_Item::Var_Item(std::string name) : Variable(ITEM)
{
    _item_name = name;
    _var_defined = true;
}

std::string Var_Item::get_Item_Name()
{
    return _item_name;
}

void Var_Item::set_Item_Name(std::string name)
{
    _item_name = name;
    _var_defined = true;
}

bool Var_Item::operator==(const Var_Item &other)const
{
    bool ans = false;

    if(this->_item_name.compare(other._item_name) == 0)
    {
        ans = true;
    }

    return ans;
}

bool Var_Item::operator!=(const Var_Item &other)const
{
    return !(*this == other);
}
