#ifndef VAR_ITEM_H
#define VAR_ITEM_H

#include <string>

#include "../Base_Objects/Variable.h"

///

class Var_Item : public Variable {
	public:
        Var_Item();
        Var_Item(std::string name);

        std::string get_Item_Name();
        void set_Item_Name(std::string name);

        bool operator==(const Var_Item &other)const;
        bool operator!=(const Var_Item &other)const;

    private:
        std::string _item_name;
};

#endif

