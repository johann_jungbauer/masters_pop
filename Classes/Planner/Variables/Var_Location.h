#ifndef VAR_LOCATION_H
#define VAR_LOCATION_H

#include <string>

#include "../Base_Objects/Variable.h"
#include "../../Common/Coord.h"

///

class Var_Location : public Variable {
	public:
        Var_Location();
        Var_Location(Coord loc);

        Coord get_Location_Coord();
        void set_Location_Coord(Coord loc);

        bool operator==(const Var_Location &other)const;
        bool operator!=(const Var_Location &other)const;

    private:
        Coord _location;
};

#endif
