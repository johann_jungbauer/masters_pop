#include "Var_Location.h"

Var_Location::Var_Location() : Variable(LOCATION)
{
    _location = Coord(0,0);
    _var_defined = false;
}

Var_Location::Var_Location(Coord loc) : Variable(LOCATION)
{
    _location = loc;
    _var_defined = true;
}

Coord Var_Location::get_Location_Coord()
{
    return _location;
}

void Var_Location::set_Location_Coord(Coord loc)
{
    _location = loc;
    _var_defined = true;
}

bool Var_Location::operator==(const Var_Location &other)const
{
    bool ans = false;

    if(this->_location == other._location)
    {
        ans = true;
    }

    return ans;
}

bool Var_Location::operator!=(const Var_Location &other)const
{
    return !(*this == other);
}


