#ifndef MEDIATOR_VARIABLE_H
#define MEDIATOR_VARIABLE_H

#include <iostream>
#include <vector>
#include <cstdlib>
#include <time.h>

#include "../../Common/log.h"
#include "../../Common/Common.h"

#include "../Enums.h"

#include "../Base_Objects/Variable.h"
#include "../Variables/Var_Item.h"
#include "../Variables/Var_Location.h"

/********************************************//**
 * \brief
 ***********************************************/
class Mediator_Variable {
	public:
        Mediator_Variable();

        void copyVariable(Variable* source, Variable* destination);
        void allocateMemory(Variable_Type type, Variable*& var);

        bool varEquality(Variable* v1, Variable* v2);

        std::string printVar(Variable* var);
        std::string printVarType(Variable_Type type);

    private:


};

#endif


