#ifndef MEDIATOR_ALGORITHM_H
#define MEDIATOR_ALGORITHM_H

#include <iostream>
#include <vector>
#include <cstdlib>
#include <time.h>

#include "../../Common/log.h"

#include "../Base_Objects/Operator.h"

#include "../Algorithm_Node.h"
#include "../Enums.h"
#include "../Plan.h"
#include "../Link.h"
#include "../Order.h"

/********************************************//**
 * \brief Structure used for the precondition lookup table.
 ***********************************************/
struct PreconOperators
{
    Predicate_Type precon;
    std::vector<Operator_Type> linkedOps;
};

/********************************************//**
 * \brief Mediator_Algorithm class is used to separate the domain from the algorithm.
 * All domain specific actions, casts, edits etc are performed here.
 ***********************************************/
class Mediator_Algorithm {
	public:
        static std::vector<PreconOperators> _preconLookUpTable;

        Mediator_Algorithm();

        bool findOperatorFirst(const Predicate_Type precon_type, Operator_Type &op_type);
        void addOperator(Operator_Type op_type, Predicate* c_pred, Plan &current_plan, Link &used_link, const int &step_index, const int &precon_index, int &current_node_index, std::vector<Algorithm_Node> &tree, std::vector<Link> &failed_links);
        int add_Plan_To_Tree(Plan &current_plan, std::vector<Algorithm_Node> &tree, int parent_index, std::vector<Link> failed_links, int seed_step_index, int seed_precon_index, Operator seed_operator);

        std::string printPreconLookupTable();

    private:
        void preconTableSetup();
        void generalInfoAddition(Plan &current_plan, Link &used_link, const int in, const int step_index, const int precon_index);

};

#endif

