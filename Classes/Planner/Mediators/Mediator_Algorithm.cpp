#include "Mediator_Algorithm.h"

std::vector<PreconOperators> Mediator_Algorithm::_preconLookUpTable;

/********************************************//**
 * \brief This is the constructor for the Mediator_Algorithm class.
 ***********************************************/
Mediator_Algorithm::Mediator_Algorithm()
{
    if(_preconLookUpTable.empty())
    {
        preconTableSetup();
    }
}

/********************************************//**
 * \brief Sets up the static lookup table used to match precondition Predicates to Operators that fulfill them.
 *
 * \return void
 *
 ***********************************************/
void Mediator_Algorithm::preconTableSetup()
{
    LOG(FILE_LOG(logDEBUG1) << "Mediator_Algorithm::preconTableSetup() -> begin";);
    //At -> Go
    PreconOperators po_AT;
    po_AT.precon = AT;
    po_AT.linkedOps.push_back(GO);

    _preconLookUpTable.push_back(po_AT);

    //Sells .... only start, thus no push_back()
    PreconOperators po_SELLS;
    po_SELLS.precon = SELLS;

    _preconLookUpTable.push_back(po_SELLS);

    //Have -> Buy
    PreconOperators po_HAVE;
    po_HAVE.precon = HAVE;
    po_HAVE.linkedOps.push_back(BUY);

    _preconLookUpTable.push_back(po_HAVE);

    LOG(FILE_LOG(logDEBUG1) << "Mediator_Algorithm::preconTableSetup() -> end";);
}

/********************************************//**
 * \brief Finds the first Operator that fulfills the given precon type.
    Returns true if an Operator is found.
    Sets op_type to the Operator that was chosen.
 *
 * \param precon_type const Predicate_Type
 * \param op_type Operator_Type&
 * \return bool
 *
 ***********************************************/
bool Mediator_Algorithm::findOperatorFirst(const Predicate_Type precon_type, Operator_Type &op_type)
{
    LOG(FILE_LOG(logDEBUG1) << "Mediator_Algorithm::findOperatorFirst() -> begin";);

    bool found = false;
    unsigned int counter = 0;

    //std::cout << "\nHELP\n" << printPreconLookupTable() << std::endl;

    //std::cout << "\nInput " << (int) precon_type << std::endl;

    while((found == false) && (counter < _preconLookUpTable.size()))
    {
        if((_preconLookUpTable[counter].precon == precon_type) && (_preconLookUpTable[counter].linkedOps.empty() == false))
        {
            op_type = _preconLookUpTable[counter].linkedOps[0];
            found = true;
        }

        counter++;
    }

    LOG(FILE_LOG(logDEBUG1) << "Mediator_Algorithm::findOperatorFirst() -> end";);

    //std::cout << "\nViable?? = " << found << std::endl;

    return found;
}

void Mediator_Algorithm::generalInfoAddition(Plan &current_plan, Link &used_link, const int in, const int step_index, const int precon_index)
{
    used_link = Link(in,step_index,precon_index);
    current_plan.add_Link(used_link);
    current_plan.add_Ordering(Order(in, step_index));
    current_plan.add_Ordering(Order(0,in));
    current_plan.add_Ordering(Order(in, 1));
}

void Mediator_Algorithm::addOperator(Operator_Type op_type, Predicate* c_pred, Plan &current_plan, Link &used_link, const int &step_index,
                           const int &precon_index, int &current_node_index, std::vector<Algorithm_Node> &tree, std::vector<Link> &failed_links)
{
    switch(c_pred->get_Predicate_Type())
    {
        case AT:
        {
            /// Op_Go
            LOG(FILE_LOG(logDEBUG1) << "AT type match";);
            Var_Location *l = dynamic_cast<Var_Location *>(c_pred->get_Variable(0));

            /// true coz we have a precon destination value and are filling in an effect destination value (ie the "there" value)
            Op_Go go = Op_Go(l->get_Location_Coord(), true);

            /// add new operator to current plan
            int in = current_plan.add_Step(go);

            generalInfoAddition(current_plan, used_link, in, step_index, precon_index);

            /// create new search tree node for the new current plan
            // point _current_index at the new node
            current_node_index = add_Plan_To_Tree(current_plan, tree, current_node_index, failed_links, step_index, precon_index, go);
            // add child index to parent
            int parent = tree[current_node_index]._parent_ID;
            tree[parent]._children.push_back(current_node_index);
        }
        break;

        case SELLS:
        {
            /// only start can implement this right now (ie in Steps())
            LOG(FILE_LOG(logDEBUG1) << "SELLS type match";);
        }
        break;

        case HAVE:
        {
            /// Op_Buy
            LOG(FILE_LOG(logDEBUG1) << "HAVE type match";);
            Var_Item *it = dynamic_cast<Var_Item *>(c_pred->get_Variable(0));
            Op_Buy buy = Op_Buy(it->get_Item_Name());

            /// add new operator to current plan
            int in = current_plan.add_Step(buy);

            generalInfoAddition(current_plan, used_link, in, step_index, precon_index);

            /// create new search tree node for the new current plan
            // point _current_index at the new node
            current_node_index = add_Plan_To_Tree(current_plan, tree, current_node_index, failed_links, step_index, precon_index, buy);
            // add child index to parent
            int parent = tree[current_node_index]._parent_ID;
            tree[parent]._children.push_back(current_node_index);
        }
        break;

        default:
        {
            /// if you reach here FAIL!!
            LOG(FILE_LOG(logDEBUG1) << "super FAIL match - no operator is available to fulfill selected precondition";);
        }
    }
}


/********************************************//**
 * \brief Used to add a plan and its relevant supporting info to the search tree.
    Returns node ID of added plan.
 *
 * \param current_plan Plan&
 * \param tree std::vector<Algorithm_Node>&
 * \param parent_index int
 * \param failed_links std::vector<Link>
 * \param seed_step_index int
 * \param seed_precon_index int
 * \param seed_operator Operator
 * \return int
 *
 ***********************************************/
int Mediator_Algorithm::add_Plan_To_Tree(Plan &current_plan, std::vector<Algorithm_Node> &tree, int parent_index, std::vector<Link> failed_links, int seed_step_index, int seed_precon_index, Operator seed_operator)
{
    LOG(FILE_LOG(logDEBUG1) << "Mediator_Algorithm::add_Plan_To_Tree() -> begin";);

    Algorithm_Node node;

    node._plan = current_plan;
    node._node_ID = tree.size();
    node._parent_ID = parent_index;
    node._failed_links = failed_links;
    node._seed_step_index = seed_step_index;
    node._seed_precon_index = seed_precon_index;
    node._failed_path = false;
    node._seed_operator = seed_operator;

    tree.push_back(node);

    LOG(FILE_LOG(logDEBUG1) << "Mediator_Algorithm::add_Plan_To_Tree() -> end";);

    return node._node_ID;
}

/********************************************//**
 * \brief Used to output a representation of the lookup table.
        The precondition Predicate and Operator enumerated types a shown by their integer values.
 *
 * \return std::string
 *
 ***********************************************/
std::string Mediator_Algorithm::printPreconLookupTable()
{
    LOG(FILE_LOG(logDEBUG1) << "Mediator_Algorithm::printPreconLookupTable() -> begin";);

    std::string out = "";
    Common comm;

    for(unsigned int i =0; i < _preconLookUpTable.size(); i++)
    {
        out.append("{");
        out.append(comm.intToString((int) _preconLookUpTable[i].precon));
        out.append(" - ");

        for(unsigned int j = 0; j < _preconLookUpTable[i].linkedOps.size(); j++)
        {
            out.append(comm.intToString((int) _preconLookUpTable[i].linkedOps[j]));
        }
        out.append("}\n");
    }

    LOG(FILE_LOG(logDEBUG1) << "Mediator_Algorithm::printPreconLookupTable() -> end";);

    return out;
}
