#ifndef MEDIATOR_OPERATOR_H
#define MEDIATOR_OPERATOR_H

#include <iostream>
#include <vector>

#include "../../Common/log.h"
#include "../../Common/Common.h"

#include "../Base_Objects/Operator.h"

#include "../Operators/Op_Go.h"
#include "../Operators/Op_Buy.h"

#include "Mediator_Variable.h"
#include "Mediator_Predicate.h"

#include "../Enums.h"

/********************************************//**
 * \brief
 ***********************************************/
class Mediator_Operator {
	public:
        Mediator_Operator();

        bool checkOperatorRule(Operator *op);

        std::string printOp(Operator& op);
        std::string printOpType(Operator_Type type);
    private:


};

#endif

