#include"Mediator_Predicate.h"

Mediator_Predicate::Mediator_Predicate()
{

}

void Mediator_Predicate::copyPredicate(Predicate* source, Predicate*& destination, Variable **& var_array)
{
    switch(source->get_Predicate_Type())
    {
        case AT:
        {
            Pred_At *at = dynamic_cast<Pred_At *>(source);
            Var_Location *loc = dynamic_cast<Var_Location *>(var_array[at->get_Op_index(0)]);

            destination = new Pred_At(loc);
            destination->set_Op_index(0, at->get_Op_index(0));
        }
        break;

        case HAVE:
        {
            Pred_Have *have = dynamic_cast<Pred_Have *>(source);
            Var_Item *it = dynamic_cast<Var_Item *>(var_array[have->get_Op_index(0)]);

            destination = new Pred_Have(it);
            destination->set_Op_index(0, have->get_Op_index(0));
        }
        break;

        case SELLS:
        {
            Pred_Sells *sell = dynamic_cast<Pred_Sells *>(source);
            Var_Location *loc = dynamic_cast<Var_Location *>(var_array[sell->get_Op_index(0)]);
            Var_Item *it = dynamic_cast<Var_Item *>(var_array[sell->get_Op_index(1)]);

            destination = new Pred_Sells(loc,it);
            destination->set_Op_index(0, sell->get_Op_index(0));
            destination->set_Op_index(1, sell->get_Op_index(1));
        }
        break;

        default:
            destination = new Predicate();
    }
}

std::string Mediator_Predicate::printPredType(Predicate_Type type)
{
    std::string out;
    switch(type)
    {
        case NULL_PRED:
            out = "NULL_PRED";
        break;

        case AT:
            out = "AT";
        break;

        case SELLS:
            out = "SELLS";
        break;

        case HAVE:
            out = "HAVE";
        break;

        default:
            out = "BROKEN PREDICATE";
    }

    return out;
}

std::string Mediator_Predicate::printPred(Predicate* pred)
{
    Common common;
    Mediator_Variable medVar;

    std::string out = "PRED: ";

    out.append(printPredType(pred->get_Predicate_Type()));
    out.append(" - ");
    out.append(common.intToString(pred->getPriority()));
    out.append(" - ");

    for(int i = 0; i < pred->get_Array_Size(); i++)
    {
        out.append(medVar.printVar(pred->get_Variable(i)));
        out.append(" & ");
    }

    return out;
}
