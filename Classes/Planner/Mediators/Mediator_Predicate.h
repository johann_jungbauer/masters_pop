#ifndef MEDIATOR_PREDICATE_H
#define MEDIATOR_PREDICATE_H

#include <iostream>
#include <vector>
#include <cstdlib>
#include <time.h>

#include "../../Common/log.h"
#include "../../Common/Common.h"

#include "../Base_Objects/Predicate.h"
#include "../Predicates/Pred_At.h"
#include "../Predicates/Pred_Have.h"
#include "../Predicates/Pred_Sells.h"

#include "Mediator_Variable.h"

#include "../Enums.h"

/********************************************//**
 * \brief
 ***********************************************/
class Mediator_Predicate {
	public:
        Mediator_Predicate();

        void copyPredicate(Predicate* source, Predicate*& destination, Variable **& var_array);

        std::string printPred(Predicate* pred);
        std::string printPredType(Predicate_Type type);
    private:


};

#endif
