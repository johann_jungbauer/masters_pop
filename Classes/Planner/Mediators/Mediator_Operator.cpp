#include "Mediator_Operator.h"

Mediator_Operator::Mediator_Operator()
{

}

std::string Mediator_Operator::printOp(Operator& op)
{
    Common comm;
    Mediator_Predicate medPred;
    std::string out = "OP: ";

    out.append(printOpType(op.get_Operator_Type()));

    out.append(" [");
    out.append(comm.intToString(op.get_Step_Index()));
    out.append("] ");

    out.append("\n\tPrecon list:\n\t");

    /// precon list
    for(int i = 0; i < op.get_Precon_Array_Size(); i++)
    {
        out.append(medPred.printPred(op.get_Precon(i)));
        out.append("\n");
    }

    out.append("\n\tAddition list:\n\t");

    /// addition list
    for(int i = 0; i < op.get_Addition_Array_Size(); i++)
    {
        out.append(medPred.printPred(op.get_Addition(i)));
        out.append("\n");
    }

    /// remove list
    out.append("\n\tRemove list:\n\t");
    for(int i = 0; i < op.get_Remove_Array_Size(); i++)
    {
        out.append(medPred.printPred(op.get_Remove(i)));
        out.append("\n");
    }

    return out;
}

std::string Mediator_Operator::printOpType(Operator_Type type)
{
    std::string out = "";

    switch(type)
    {
        case START:
            out = "START";
        break;

        case FINISH:
            out = "FINISH";
        break;

        case GO:
            out = "GO";
        break;

        case BUY:
            out = "BUY";
        break;

        default:
            out = "BROKEN OPERATOR";
    }

    return out;
}

bool Mediator_Operator::checkOperatorRule(Operator *op)
{
    LOG(FILE_LOG(logDEBUG1) << "Mediator_Algorithm::checkOperatorRule() -> begin";);

    bool rulePass = false;

    /// dynamically cast based on type
    switch(op->get_Operator_Type())
    {
        case GO:
        {
            //Op_Go *go = dynamic_cast<Op_Go *>(op);
            Op_Go *go = (Op_Go *) op;

            if(go->variable_Rule_Check() == false)
            {
                LOG(FILE_LOG(logDEBUG1) << "variable inconsistency found";);
                rulePass = false;
            }
            else
            {
                rulePass = true;
            }
        }
        break;

        default:
        break;
    }

    LOG(FILE_LOG(logDEBUG1) << "Mediator_Algorithm::checkOperatorRule() -> end";);

    return rulePass;
}
