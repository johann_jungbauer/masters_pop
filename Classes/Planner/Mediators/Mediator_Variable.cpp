#include"Mediator_Variable.h"

Mediator_Variable::Mediator_Variable()
{

}

/********************************************//**
 * \brief
 *
 * \param source Variable*
 * \param destination Variable*
 * \return void
 *
 ***********************************************/
void Mediator_Variable::copyVariable(Variable* source, Variable* destination)
{
    switch(source->get_Type())
    {
        case ITEM:
        {
            Var_Item *itS = dynamic_cast<Var_Item *>(source);
            Var_Item *itD = dynamic_cast<Var_Item *>(destination);

            itD->set_Item_Name(itS->get_Item_Name());
            itD->set_Defined(itS->get_Defined());
        }
        break;

        case LOCATION:
        {
            Var_Location *locS = dynamic_cast<Var_Location *>(source);
            Var_Location *locD = dynamic_cast<Var_Location *>(destination);

            locD->set_Location_Coord(locS->get_Location_Coord());
            locD->set_Defined(locS->get_Defined());
        }
        break;

        default:
        {
            //destination = new Variable();
        }
        break;
    }
}

/********************************************//**
 * \brief
 *
 * \param type Variable_Type
 * \param var Variable*&
 * \return void
 *
 ***********************************************/
void Mediator_Variable::allocateMemory(Variable_Type type, Variable*& var)
{
    switch(type)
        {
            case ITEM:
            {
                var = new Var_Item();
            }
            break;

            case LOCATION:
            {
                var = new Var_Location();
            }
            break;

            default:
            {
                var = new Variable();
            }
            break;
        }
}

/********************************************//**
 * \brief This method takes in a Variable pointer, casts it to the child type and returns info as a std::string.
 *
 * \param var Variable*
 * \return std::string
 *
 ***********************************************/
std::string Mediator_Variable::printVar(Variable* var)
{
    Common common;

    std::string out = "VAR: ";

    out.append(printVarType(var->get_Type()));

    out.append(" ");
    out.append(common.boolToString(var->get_Defined()));
    out.append(" ");

    switch(var->get_Type())
    {
        case ITEM:
        {
            Var_Item *it = dynamic_cast<Var_Item *>(var);
            out.append(" - ");
            out.append(it->get_Item_Name());
        }
        break;

        case LOCATION:
        {
            Var_Location *loc = dynamic_cast<Var_Location *>(var);
            out.append(" - ");
            out.append(loc->get_Location_Coord().print());
        }
        break;

        default:
        {
            out.append("default junk");
        }
    }

    return out;
}

/********************************************//**
 * \brief This method takes in two variable pointers, dynamic casts to child type and compares their equality.
 *
 * \param v1 Variable*
 * \param v2 Variable*
 * \return bool
 *
 ***********************************************/
bool Mediator_Variable::varEquality(Variable* v1, Variable* v2)
{
    bool ans = false;

    // check that the types match first
    if(v1->get_Type() == v2->get_Type())
    {
        switch(v1->get_Type())
        {
            case ITEM:
            {
                Var_Item *it = dynamic_cast<Var_Item *>(v1);
                Var_Item *ito = dynamic_cast<Var_Item *>(v2);
                if(*it == *ito)
                {
                    ans = true;
                }
            }
            break;

            case LOCATION:
            {
                Var_Location *l = dynamic_cast<Var_Location *>(v1);
                Var_Location *lo = dynamic_cast<Var_Location *>(v2);
                if(*l == *lo)
                {
                    ans = true;
                }
            }
            break;

            default:
            break;
        }
    }

    return ans;
}

std::string Mediator_Variable::printVarType(Variable_Type type)
{
    std::string out;
    switch(type)
    {
        case ITEM:
            out = "ITEM";
        break;

        case LOCATION:
            out = "LOCATION";
        break;

        case NULL_VAR:
            out = "NULL_VAR";
        break;

        default:
            out = "BROKEN VAR";
    }

    return out;
}
