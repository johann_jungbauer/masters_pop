#ifndef ENUMS_H
#define ENUMS_H

enum Operator_Type
{
    NULL_OP = 0,
    START = 1,
    FINISH = 2,
    GO = 3,
    BUY = 4
};

enum Predicate_Type
{
    NULL_PRED = 0,
    AT = 1,
    SELLS = 3,
    HAVE = 4
};

enum Variable_Type
{
    NULL_VAR = 0,
    ITEM = 1,
    LOCATION = 2
};

#endif
