#ifndef POP_ALGORITHM_H
#define POP_ALGORITHM_H

#include <algorithm>
#include <vector>

#include "Plan.h"
#include "Base_Objects/Predicate.h"
#include "Base_Objects/Operator.h"
#include "Link.h"

#include "Operators/Op_Start.h"
#include "Operators/Op_Finish.h"
#include "Operators/Op_Buy.h"
#include "Operators/Op_Go.h"

#include "Algorithm_Node.h"
#include "Mediators/Mediator_Algorithm.h"
#include "Mediators/Mediator_Variable.h"
#include "Mediators/Mediator_Predicate.h"
#include "Mediators/Mediator_Operator.h"

#include "../Common/log.h"
#include "../Common/Common.h"



/********************************************//**
 * \brief The class that controls the Partially Ordered Planner (POP).
 ***********************************************/
class POP_Algorithm {
	public:
        POP_Algorithm();

        // passing vectors by reference to save memory
        // passing vectors as const because I don't need to change them
        bool run_Algorithm(const std::vector<Predicate *> &initial_state, const int initial_var_num, const std::vector<Predicate *> &goal_state, const int goal_var_num, std::vector<Operator *> operators, Plan &plan);
        void make_Minimal_Plan(const std::vector<Predicate *> &initial_state, const int &initial_var_num, const std::vector<Predicate *> &goal_state, const int &goal_var_num);

        void record_Current_Plan();
        bool select_Subgoal(int &step_index, int &precon_index);
        bool solution(bool &plan_found);

        bool choose_Operator(std::vector<Operator *> operators, int step_index, int precon_index, Link &used_link);
        bool choose_Op_Steps_Defined_Precon(Predicate* c_pred, const Predicate_Type precon_type, const int &step_index, const int &precon_index, Link &used_link);
        bool choose_Op_Ops_Defined_Precon(Predicate* c_pred, const Predicate_Type precon_type, const int &step_index, const int &precon_index, Link &used_link);

        bool search_Failed_Links(Link element);

        bool resolve_Threats();
        bool consistent();
        bool var_consistent();
        bool order_consistent();

        void internal_Rollback(Link &used_link);
        void parent_Rollback();

        std::string print_Tree(bool full_info);
        void co_info(const int &step_index, const int &precon_index, Predicate* c_pred);

    private:
        Plan _current_plan;
        int _current_node_index;
        Plan _previous_plan;
        bool _step_addition;

        std::vector<Operator> _operators;
        std::vector<Predicate> _initial_state;
        std::vector<Predicate> _goal_state;
        std::vector<Algorithm_Node> _tree;
        std::vector<Link> _failed_links;

};

#endif
