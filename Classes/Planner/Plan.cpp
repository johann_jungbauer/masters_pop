#include "Plan.h"

Plan::Plan()
{

}


/// assignment operator
/*Plan& Plan::operator= (const Plan &plan_source)
{
    /// Links
    _links.clear();
    for(int i=0; i < plan_source._links.size(); i++)
    {
        _links.push_back(plan_source._links[i]);
    }

    /// Ordering
    _ordering.clear();
    for(int i=0; i < plan_source._ordering.size(); i++)
    {
        _ordering.push_back(plan_source._ordering[i]);
    }

    /// precon tracker
    _precon_tracker.clear();
    for(int i=0; i < plan_source._precon_tracker.size(); i++)
    {
        _precon_tracker.push_back(plan_source._precon_tracker[i]);
    }

    /// Steps
    _steps.clear();
    std::vector<Operator>::const_iterator iter;
    for(iter = plan_source._steps.begin(); iter != plan_source._steps.end(); iter++)
    {
        _steps.push_back(*iter);
    }


    return *this;
}*/

int Plan::add_Step(Operator step)
{

    //FILE_LOG(logDEBUG1) << "%%%%%%%%" << step.print();
    int size = _steps.size();
    step.set_Step_Index(size);

    _steps.push_back(step);

    //FILE_LOG(logDEBUG1) << "^^^^^^^^^^" <<_steps[size].print();

    /// precon tracking -- adding new preconditions
    /// iterates through the operator precon list and adds all predicates
    for(int i = 0; i < step.get_Precon_Array_Size(); i++)
    {
        Predicate *ptr = step.get_Precon(i);

        if(ptr->get_Predicate_Type() != NULL_PRED)
        {
            _precon_tracker.push_back(Triplet(step.get_Step_Index(),i, ptr->getPriority()));
        }
    }

    /// sort precon tracker by priority
    //std::sort(_precon_tracker.begin(), _precon_tracker.end());
    //std::reverse(_precon_tracker.begin(), _precon_tracker.end());

    return step.get_Step_Index();
}

void Plan::sort_Precon_Tracker()
{
    calculate_Priorities();
    std::sort(_precon_tracker.begin(), _precon_tracker.end());
    std::reverse(_precon_tracker.begin(), _precon_tracker.end());
}

void Plan::calculate_Priorities()
{
    /// set STEP priorities to zero
    for(unsigned int i =0; i < _steps.size(); i++)
    {
        _steps[i].set_Priority(0);
    }

    /// loop through links to increment STEP priorities
    for(unsigned int loop = 0; loop < _links.size(); loop++)
    {
        int step = _links[loop].get_Effect_Step_Index();
        _steps[step].mod_Priority(1);
    }

    /// add the priorities of linked steps
    for(unsigned int l = 0; l < _links.size(); l++)
    {
        int step1 = _links[l].get_Effect_Step_Index();
        int step2 = _links[l].get_Precon_Step_Index();

        _steps[step1].mod_Priority(_steps[step2].get_Priority());
    }

    /// loop through tracker to set precon priorities
    for(unsigned int x = 0; x < _precon_tracker.size(); x++)
    {
        int step = _precon_tracker[x].getX();
        int precon = _precon_tracker[x].getY();

        int s_prior = _steps[step].get_Priority();
        int s_pun = _steps[step].get_Punishment();
        int p_prior = _steps[step].get_Precon(precon)->getPriority();

        _precon_tracker[x].setZ(s_prior + p_prior - s_pun);
    }
}

void Plan::add_Link(Link link)
{
    _links.push_back(link);

    /// Links define achieved preconditions thus,
    /// associated precons can be removed from the tracker
    vector<Triplet>::iterator iter;
    iter = _precon_tracker.begin();
    Coord cLink = Coord(link.get_Precon_Step_Index(), link.get_Precon_Index());
    bool found = false;
    while((!found) && iter != _precon_tracker.end())
    {
        Coord check = Coord((*iter).getX(), (*iter).getY());
        if(check == cLink)
        {
           found = true;
           _precon_tracker.erase(iter);
        }

        iter++;
    }
}

void Plan::add_Ordering(Order ordering)
{
    _ordering.push_back(ordering);
}

std::string Plan::print_Steps()
{
    Mediator_Operator medOp;
    std::string out;

    for(unsigned int i = 0; i < _steps.size(); i++)
    {
        out.append(medOp.printOp(_steps[i]));
        out.append("\n");
    }

    return out;
}

 std::string Plan::print_Links()
 {
     std::string out;

     for(unsigned int i = 0; i < _links.size(); i++)
    {
        out.append(_links[i].print());
        out.append("\n");
    }

    return out;
 }

 std::string Plan::print_Ordering()
 {
     std::string out;

     for(unsigned int i = 0; i < _ordering.size(); i++)
    {
        out.append(_ordering[i].print());
        out.append("\n");
    }

    return out;
 }

std::string Plan::print_Precon_Tracker()
{
    std::string out = "_Precon_Tracker ";

    Common comm;

    out.append(comm.intToString(_precon_tracker.size()));
    out.append("\n\t");

    for(unsigned int i = 0; i < _precon_tracker.size(); i++)
    {
        out.append(_precon_tracker[i].print());
        out.append("\n\t");
    }

    return out;
}

std::string Plan::print()
{
    std::string out = "STEPS: \n";
    out.append(print_Steps());
    out.append("\nLINKS: \n");
    out.append(print_Links());
    out.append("\nORDERING: \n");
    out.append(print_Ordering());
    out.append("\nPRECON TRACKER: \n");
    out.append(print_Precon_Tracker());
    return out;
}

bool Plan::find_Order(Order ord)
{
    bool found = false;

    if(std::find(_ordering.begin(), _ordering.end(), ord) != _ordering.end())
    {
        found = true;
    }

    return found;
}
