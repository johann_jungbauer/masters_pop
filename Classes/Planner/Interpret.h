#ifndef INTERPRET_H
#define INTERPRET_H

#include <iostream>
#include <vector>
#include <cstdlib>
#include <time.h>

#include "../Common/log.h"
#include "../Common/Pair.h"
#include "../Common/Common.h"

///
class Interpret {
	public:
        Interpret();

        void doInterpret(vector<Pair> &ordering, vector<int> &totalOrder);

        void populateVector(vector<Pair> &vec);
        void populateVector2(vector<Pair> &vec);
        void cullVector(vector<Pair> &vec);

        void interpretPOP(vector<Pair> &oldVec, vector<vector<int> > &newVec);
        bool fulfilledCheck(Pair &used, vector<vector<int> > &vec);
        bool deletePair(Pair item, vector<Pair> &vec);
        void placePair(Pair &used, vector<vector<int> > &vec);
        void orderTies(vector<vector<int> > &vec, vector<int> &ordered);

        /// Search methods
        bool searchFirstPosition(int marked, vector<Pair> &vec, Pair &ans);
        bool searchSecondPosition(int marked, vector<Pair> &vec, Pair &ans);
        bool search2DVector(int mark, Pair &pos, vector<vector<int> > &vec);
        bool search2DVector(int mark, int &pos, vector<vector<int> > &vec);

        /// Display methods
        void displayPairVector(vector<Pair> &vec);
        void display2DIntVector(vector<vector<int> > &vec);
        void displayIntVector(vector<int> &vec);

    private:

};

#endif
