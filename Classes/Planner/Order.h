#ifndef ORDER_H
#define ORDER_H

#include "../Common/Common.h"

/// The Order class is used to store the temporal links between steps.

/// _preceeding_Step_Index : the index of the step that must come before _following_Step_Index.
/// _following_Step_Index : the index of the step that must come after _preceeding_Step_Index.

class Order {
	public:
        Order(int preceeding ,int following);
        Order();

        int get_Preceeding_Step_Index();
        void set_Preceeding_Step_Index(int index);
        int get_Following_Step_Index();
        void set_Following_Step_Index(int index);

        bool operator==(const Order &other)const;
        bool operator!=(const Order &other)const;

        std::string print();

    private:
        int _preceeding_Step_Index;
        int _following_Step_Index;
};

#endif
