#ifndef OPERATOR_H
#define OPERATOR_H

#include <string>

#include "../Enums.h"
#include "Variable.h"
#include "Predicate.h"

#include "../../Common/log.h"

#include "../Mediators/Mediator_Variable.h"
#include "../Mediators/Mediator_Predicate.h"

///

class Operator {
	public:
        Operator();
        Operator(Operator_Type type, int var_num, int precon_num, int addition_num, int remove_num, bool var_rule);
        ~Operator();

        static Predicate p1;

        /// copy constructor
        Operator(const Operator &op_source);

        /// assignment operator
        Operator& operator= (const Operator &op_source);

        int get_Precon_Array_Size();
        int get_Addition_Array_Size();
        int get_Remove_Array_Size();
        int get_Var_Array_Size();

        /// returns the position of the Operator in the algorithm Steps() array
        int get_Step_Index();
        void set_Step_Index(int index);

        Operator_Type get_Operator_Type();

        /// returns the value at the indicated index
        /// the arrays hold pointers thus pointers are returned
        Predicate* get_Precon(int index);
        Predicate* get_Addition(int index);
        Predicate* get_Remove(int index);
        Variable* get_Variable(int index);

        /// used in consistency checks, intended to be overridden
        bool variable_Rule_Check();
        bool get_Var_Rule();

        //std::string print();
        //std::string print_Operator();

        virtual void dummy(){};

        int get_Priority();
        void set_Priority(int val);
        void mod_Priority(int val);

        int get_Punishment();
        void set_Punishment(int val);
        void mod_Punishment(int val);


    protected:
        Predicate ** _precon_array;
        Predicate ** _addition_array;
        Predicate ** _remove_array;
        Variable ** _var_array;

    private:
        bool _var_rule;
        int _var_array_size;
        int _precon_array_size;
        int _addition_array_size;
        int _remove_array_size;
        int _priority;
        int _punishment;

        int _step_index;
        Operator_Type _type;

        void do_Copy(const Operator &op_source);
        void do_Pointer_Delete();
};

#endif

