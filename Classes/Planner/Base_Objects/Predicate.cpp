#include "Predicate.h"

Variable Predicate::v1;

Predicate::Predicate() :
    _array_size(1),
    _priority(1),
    _type(NULL_PRED)
{
    _variable_array = new Variable* [_array_size];
    /// attempting to stabalize calling the is variable
    for(int i =0; i < _array_size; i++)
    {
        _variable_array[i] = &v1;
    }

    _op_index_array = new int [_array_size];

    populate_Op_index();
}

Predicate::Predicate(int arr_size, Predicate_Type type) :
    _array_size(arr_size),
    _priority(arr_size),
    _type(type)
{
    _variable_array = new Variable* [_array_size];
    _op_index_array = new int [_array_size];

    /// attempting to stabalize calling the is variable
    for(int i =0; i < _array_size; i++)
    {
        _variable_array[i] = &v1;
    }

    populate_Op_index();
}

void Predicate::populate_Op_index()
{
    for(int i = 0; i < _array_size; i++)
    {
        _op_index_array[i] = -1;
    }
}

Predicate::~Predicate()
{
    do_Pointer_Delete();
}

void Predicate::do_Pointer_Delete()
{
    delete [] _variable_array;
    delete [] _op_index_array;
}

/// copy constructor
Predicate::Predicate(const Predicate &pred_source)
{
    do_Copy(pred_source);
}

/// assignment operator
Predicate& Predicate::operator= (const Predicate &pred_source)
{
    Mediator_Variable medVar;
    /// I'm assuming that the data structures exist thus I'm only changing values

    /// both already the same
    //_array_size = pred_source._array_size;
    //_type = pred_source._type;

    /// using existing data structures
    //_variable_array = new Variable* [_array_size];
    //_op_index_array = new int [_array_size];

    for(int i = 0; i < _array_size; i++)
    {
        /// keeping the operators internal reference the same
        //_op_index_array[i] = pred_source._op_index_array[i];
        //*_variable_array[i] = *pred_source._variable_array[i];

        medVar.copyVariable(pred_source._variable_array[i],_variable_array[i]);
        /*switch(pred_source._variable_array[i]->get_Type())
        {
            case ITEM:
            {
                Var_Item *it_s = dynamic_cast<Var_Item *>(pred_source._variable_array[i]);
                Var_Item *it = dynamic_cast<Var_Item *>(_variable_array[i]);

                *it = *it_s;
            }
            break;

            case LOCATION:
            {
                Var_Location *l_s = dynamic_cast<Var_Location *>(pred_source._variable_array[i]);
                Var_Location *l = dynamic_cast<Var_Location *>(_variable_array[i]);

                *l = *l_s;
            }
            break;

            default:
                _variable_array[i] = new Variable();
        }*/
    }

    return *this;
}

bool Predicate::get_Var_Defined(int &count)
{
    bool defined = true;
    int defined_count = 0; // counts the number of defined variables

    /// loop through the predicate variables to see if they are all defined
    for(int i = 0; i < _array_size; i++)
    {
        if(_variable_array[i]->get_Defined() == false)
        {
            defined = false;
        }
        else
        {
            // variable is defined thus increment counter
            defined_count++;
        }
    }

    count = defined_count;
    return defined;
}

bool Predicate::get_Var_Defined()
{
    int temp = 0;
    return get_Var_Defined(temp);
}

void Predicate::do_Copy(const Predicate &pred_source)
{
    // TODO (code#1#) Must figure out how to do the pointer deletion properly.
    /// deletes pointers before new assignments
    //do_Pointer_Delete();

    _array_size = pred_source._array_size;
    _type = pred_source._type;
    _priority = pred_source._priority;

    _variable_array = new Variable* [_array_size];
    _op_index_array = new int [_array_size];

    for(int i = 0; i < _array_size; i++)
    {
        // TODO (weird#1#) I have no idea why do_Copy and = can't be the same...
        _op_index_array[i] = pred_source._op_index_array[i];
        *_variable_array[i] = *pred_source._variable_array[i];
        /* possibe
        Variable* new_variable_ptr = new Variable();
        *new_variable_ptr = *(*(pred_source._variable_array)[i]);
        *(_variable_array)[i] = new_variable_ptr;

        */
        /*switch(pred_source._variable_array[i]->get_Type())
        {
            case ITEM:
            {
                Var_Item *it = dynamic_cast<Var_Item *>(pred_source._variable_array[i]);
                _variable_array[i]
            }
            break;

            case LOCATION:
            {
                Var_Location *l = dynamic_cast<Var_Location *>(pred_source._variable_array[i]);
            }
            break;

            default:
                _variable_array[i] = new Variable();
        }*/
    }

}

int Predicate::get_Array_Size()
{
    return _array_size;
}

Variable* Predicate::get_Variable(int index)
{
    return _variable_array[index];
}

Predicate_Type Predicate::get_Predicate_Type()
{
    return _type;
}

bool Predicate::operator==(const Predicate &other)const
{
    Mediator_Variable medVar;
    bool ans = true;

    if(this->_type != other._type)
    {
        /// types do not match
        ans = false;
    }
    else
    {
        /// types match. now checking if the variables match up
        for(int i = 0; i < this->_array_size; i++)
        {
            ans = medVar.varEquality(this->_variable_array[i],other._variable_array[i]);
            /*switch(this->_variable_array[i]->get_Type())
            {
                case ITEM:
                {
                    Var_Item *it = dynamic_cast<Var_Item *>(this->_variable_array[i]);
                    Var_Item *ito = dynamic_cast<Var_Item *>(other._variable_array[i]);
                    if(*it != *ito)
                    {
                        ans = false;
                        /// really want to add a break statment here
                    }
                }
                break;

                case LOCATION:
                {
                    Var_Location *l = dynamic_cast<Var_Location *>(this->_variable_array[i]);
                    Var_Location *lo = dynamic_cast<Var_Location *>(other._variable_array[i]);
                    if(*l != *lo)
                    {
                        ans = false;
                    }
                }
                break;

                default:
                    ans = false;
            }*/
        }
    }

    return ans;
}

bool Predicate::operator!=(const Predicate &other)const
{
    return !(*this == other);
}

int Predicate::get_Op_index(int var_pos)
{
    return _op_index_array[var_pos];
}

void Predicate::set_Op_index(int var_pos, int op_index)
{
    _op_index_array[var_pos] = op_index;
}

int Predicate::getPriority()
{
    return _priority;
}

void Predicate::setPriority(int priority)
{
    _priority = priority;
}
