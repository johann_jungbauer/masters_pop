#include "Operator.h"

Predicate Operator::p1;

Operator::Operator():
    _var_rule(false),
    _var_array_size(1),
    _precon_array_size(1),
    _addition_array_size(1),
    _remove_array_size(1),
    _type(NULL_OP)
{
    _var_array = new Variable* [_var_array_size];
    _precon_array = new Predicate* [_precon_array_size];
    _addition_array = new Predicate* [_addition_array_size];
    _remove_array = new Predicate* [_remove_array_size];
    _step_index = -1;
    _priority = 0;
    _punishment = 0;

    /// stablility
    for(int i = 0; i < _var_array_size; i++)
    {
        _var_array[i] = &Predicate::v1;
    }

    for(int i = 0; i < _precon_array_size; i++)
    {
        _precon_array[i] = &p1;
    }

    for(int i = 0; i < _addition_array_size; i++)
    {
        _addition_array[i] = &p1;
    }

    for(int i = 0; i < _remove_array_size; i++)
    {
        _remove_array[i] = &p1;
    }
}

Operator::Operator(Operator_Type type, int var_num, int precon_num, int addition_num, int remove_num, bool var_rule):
    _var_rule(var_rule),
    _var_array_size(var_num),
    _precon_array_size(precon_num),
    _addition_array_size(addition_num),
    _remove_array_size(remove_num),
    _type(type)
{
    _var_array = new Variable* [_var_array_size];
    _precon_array = new Predicate* [_precon_array_size];
    _addition_array = new Predicate* [_addition_array_size];
    _remove_array = new Predicate* [_remove_array_size];
    _step_index = -1;
    _priority = 0;
    _punishment = 0;

    /// stablility
    for(int i = 0; i < _var_array_size; i++)
    {
        _var_array[i] = &Predicate::v1;
    }

    for(int i = 0; i < _precon_array_size; i++)
    {
        _precon_array[i] = &p1;
    }

    for(int i = 0; i < _addition_array_size; i++)
    {
        _addition_array[i] = &p1;
    }

    for(int i = 0; i < _remove_array_size; i++)
    {
        _remove_array[i] = &p1;
    }
}

Operator::~Operator()
{
    do_Pointer_Delete();
}

void Operator::do_Pointer_Delete()
{
    delete [] _var_array;
    delete [] _precon_array;
    delete [] _addition_array;
    delete [] _remove_array;
}

/// copy constructor
Operator::Operator(const Operator &op_source)
{
    do_Copy(op_source);
}

/// assignment operator
Operator& Operator::operator= (const Operator &op_source)
{
    do_Copy(op_source);
    return *this;
}

void Operator::do_Copy(const Operator &op_source)
{
    //FILE_LOG(logDEBUG1) << "COPY CONSTRUCTOR";

    Mediator_Variable medVar;
    Mediator_Predicate medPred;

    /// deletes pointers before new assignments
    //do_Pointer_Delete(); // broke many things

    /// defining non-pointer variables
    _var_array_size = op_source._var_array_size;
    _precon_array_size = op_source._precon_array_size;
    _addition_array_size = op_source._addition_array_size;
    _remove_array_size = op_source._remove_array_size;
    _step_index = op_source._step_index;
    _type = op_source._type;
    _var_rule = op_source._var_rule;
    _priority = op_source._priority;
    _punishment = op_source._punishment;

    /// defining pointer variables
    _var_array = new Variable* [_var_array_size];
    _precon_array = new Predicate* [_precon_array_size];
    _addition_array = new Predicate* [_addition_array_size];
    _remove_array = new Predicate* [_remove_array_size];

    /// -- defining variable array
    for(int i = 0; i < _var_array_size; i++)
    {
        medVar.allocateMemory(op_source._var_array[i]->get_Type(), _var_array[i]);
        medVar.copyVariable(op_source._var_array[i], _var_array[i]);
    }

    /// -- defining precon array
    for(int i = 0; i < _precon_array_size; i++)
    {
        medPred.copyPredicate(op_source._precon_array[i], _precon_array[i], _var_array);
    }

    /// -- defining addition array
    for(int i = 0; i < _addition_array_size; i++)
    {
        medPred.copyPredicate(op_source._addition_array[i], _addition_array[i], _var_array);
    }

    /// -- defining remove array
    for(int i = 0; i < _remove_array_size; i++)
    {
        medPred.copyPredicate(op_source._remove_array[i], _remove_array[i], _var_array);
    }
}

int Operator::get_Precon_Array_Size()
{
    return _precon_array_size;
}

int Operator::get_Addition_Array_Size()
{
    return _addition_array_size;
}

int Operator::get_Remove_Array_Size()
{
    return _remove_array_size;
}

int Operator::get_Var_Array_Size()
{
    return _var_array_size;
}

int Operator::get_Step_Index()
{
    return _step_index;
}

void Operator::set_Step_Index(int index)
{
    _step_index = index;
}

Operator_Type Operator::get_Operator_Type()
{
    return _type;
}

Predicate* Operator::get_Precon(int index)
{
    return _precon_array[index];
}

Predicate* Operator::get_Addition(int index)
{
    return _addition_array[index];
}

Predicate* Operator::get_Remove(int index)
{
    return _remove_array[index];
}

Variable* Operator::get_Variable(int index)
{
    return _var_array[index];
}

bool Operator::variable_Rule_Check()
{return true;}

bool Operator::get_Var_Rule()
{
    return _var_rule;
}

int Operator::get_Priority()
{
    return _priority;
}

void Operator::set_Priority(int val)
{
    _priority = val;
}

void Operator::mod_Priority(int val)
{
    _priority = _priority + val;
}

int Operator::get_Punishment()
{
    return _punishment;
}

void Operator::set_Punishment(int val)
{
    _punishment = val;
}

void Operator::mod_Punishment(int val)
{
    _punishment = _punishment + val;
}
