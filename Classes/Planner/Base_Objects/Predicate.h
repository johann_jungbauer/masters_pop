#ifndef PREDICATE_H
#define PREDICATE_H

#include "../../Common/Common.h"
#include "../Enums.h"
#include "Variable.h"

#include "../Mediators/Mediator_Variable.h"

/// The Predicate class is used as the base parent class for all predicates used by the POP algorithm.

/// _array_size : defines the size of the variable array.
/// _variable_array : an array that stores the variables used by the predicate
/// _type : stores the type of predicate for comparison purposes

class Predicate {
	public:
        Predicate();
        Predicate(int arr_size, Predicate_Type type);
        ~Predicate();

        /// for initial stability
        static Variable v1;

        /// copy constructor
        Predicate(const Predicate &pred_source);

        /// assignment operator
        Predicate& operator= (const Predicate &pred_source);

        bool operator==(const Predicate &other)const;
        bool operator!=(const Predicate &other)const;

        int get_Array_Size();
        Variable* get_Variable(int index);
        Predicate_Type get_Predicate_Type();

        bool get_Var_Defined(int &count);
        bool get_Var_Defined();

        int getPriority();
        void setPriority(int priority);

        virtual void dummy(){};

        void populate_Op_index();

        /// returns the Operator variable array position of the predicate variable at the index (var_pos)
        int get_Op_index(int var_pos);

        /// sets the Operator variable array position of the predicate variable at the index (var_pos)
        /// should only be called from Operator
        void set_Op_index(int var_pos, int op_index);

    protected:
        Variable **_variable_array;

        /// holds the variable's position in the related Operator's variable array
        /// this couples Predicate to Operator
        int *_op_index_array;

    private:
        int _array_size;
        int _priority;
        Predicate_Type _type;

        void do_Copy(const Predicate &pred_source);
        void do_Pointer_Delete();
};

#endif
