#include "Variable.h"

Variable::Variable() :
    _type(NULL_VAR)
{
    _var_defined = false;
}

Variable::Variable(Variable_Type type)
{
    _type = type;
    _var_defined = false;
}

Variable_Type Variable::get_Type()
{
    return _type;
}

bool Variable::get_Defined()
{
    return _var_defined;
}

void Variable::set_Defined(bool def)
{
    _var_defined = def;
}
