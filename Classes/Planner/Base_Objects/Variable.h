#ifndef VARIABLE_H
#define VARIABLE_H

#include <string>

#include "../Enums.h"
#include "../../Common/Common.h"


/// The Variable class is used to simplify the comparison of the variables the are used in the Predicate class.
///     This allows the variables to have their type compared without the need to overwrite equallity comparison operators.

/// _type : stores the base type of the variable.

class Variable {
	public:
        Variable();
        Variable(Variable_Type type);
        Variable_Type get_Type();

        virtual ~Variable(){};

        bool get_Defined();
        void set_Defined(bool def);

    protected:
        bool _var_defined;

    private:
        Variable_Type _type;
};

#endif
