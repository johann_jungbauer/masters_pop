#ifndef PLAN_H
#define PLAN_H

#include <vector>
#include <algorithm>

#include "../Common/Triplet.h"
#include "../Common/log.h"

#include "Base_Objects/Operator.h"
#include "Link.h"
#include "Order.h"

#include "Mediators/Mediator_Operator.h"


class Plan {
	public:
        Plan();

        int add_Step(Operator step);
        void add_Link(Link link);
        void add_Ordering(Order ordering);

        void sort_Precon_Tracker();

        bool find_Order(Order ord);

        void calculate_Priorities();

        std::string print_Steps();
        std::string print_Links();
        std::string print_Ordering();

        std::string print();

        std::vector<Operator> _steps;
        std::vector<Link> _links;
        std::vector<Order> _ordering;

        /// x-int is step, y-int is precon, z-int is precon pred priority
        std::vector<Triplet> _precon_tracker;

        std::string print_Precon_Tracker();

        /// these should not be needed if I use non-pointer vectors
        /// copy constructor
        //Plan(const Plan &plan_source);

        /// assignment operator
        //Plan& operator= (const Plan &plan_source);

    private:


};

#endif
