#ifndef LINK_H
#define LINK_H

#include <string>

#include "../Common/Common.h"

/// The Link class is used to store the causal links between steps in the plan.

/// _effect_Step_Index : stores the index of the step whose effect achieves the precondition of the precon step.
/// _precon_Step_Index : stores the index of the step whose precondition the effect step achieves.
/// _precon_Index : stores the index of the specific precondition within the precon step.

class Link {
	public:
        Link(int effect_step ,int precon_step, int precon_index);
        Link();

        int get_Effect_Step_Index();
        void set_Effect_Step_Index(int index);

        int get_Precon_Step_Index();
        void set_Precon_Step_Index(int index);

        int get_Precon_Index();
        void set_Precon_Index(int index);

        std::string print();

        bool operator==(const Link &other)const;
        bool operator!=(const Link &other)const;

    private:
        int _effect_Step_Index;
        int _precon_Step_Index;
        int _precon_Index;
};

#endif
