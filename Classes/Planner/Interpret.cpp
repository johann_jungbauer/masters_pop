#include "Interpret.h"

Interpret::Interpret()
{

}

void Interpret::doInterpret(vector<Pair> &ordering, vector<int> &totalOrder)
{
    srand(time(0));

    //vector<Pair> ordering;
    //populateVector2(ordering);
    //displayPairVector(ordering);
    cullVector(ordering);
    //displayPairVector(ordering);

    //vector<int> totalOrder;
    vector<vector<int> > nearOrdered;
    interpretPOP(ordering, nearOrdered);

    //FILE_LOG(logDEBUG2) << "\n Nearly ordered";
    //display2DIntVector(nearOrdered);
    orderTies(nearOrdered, totalOrder);

    LOG(FILE_LOG(logDEBUG2) << "\n Ordered";);
    displayIntVector(totalOrder);
}

void Interpret::interpretPOP(vector<Pair> &oldVec, vector<vector<int> > &newVec)
{
    LOG(FILE_LOG(logDEBUG) << "interpretPOP() --> begin";);

    bool ended = false;
    vector<Pair> editVec = oldVec;
    vector<int> working;
    Pair used = Pair(0,0);
    //displayVector(editVec);

    /// place first pair
    used = editVec[0];
    working.push_back(used.getX());
    newVec.push_back(working);
    working[0] = used.getY();
    newVec.push_back(working);

    /// first pair is fulfulled thus delete
    deletePair(used, editVec);

    //display2DIntVector(newVec);

    /// set marked operation
    int marked = used.getY();

    //displayPairVector(editVec);
    //display2DIntVector(newVec);

    while((ended == false) && (editVec.size() > 0))
    {
        LOG(FILE_LOG(logDEBUG1) << "-- Iteration start --\tMarked = " << marked;);
        //display2DIntVector(newVec);

        /// search for marked as first in pairing
        if(searchFirstPosition(marked, editVec, used))
        {
            LOG(FILE_LOG(logDEBUG2) << "marked found, used = " << used.print(););

            /// checking if the pairing order is already fulfilled
            if(fulfilledCheck(used, newVec))
            {
                LOG(FILE_LOG(logDEBUG2) << "Already fulfilled : " << used.print(););
                marked = used.getY();
                deletePair(used, editVec);
            }
            else
            {
                marked = used.getY();
                placePair(used, newVec);
                deletePair(used, editVec);
            }
        }
        else
        {
            LOG(FILE_LOG(logDEBUG2) << "marked not found, step up or across";);
            Pair pos;
            search2DVector(marked, pos, newVec);
            /// pos is the position of marked in the 2D array (outer,inner)

            /// if I can step across, do that ie array hase more than 1 element && mark's pos is not the last in array
            /// else step up
            if((newVec[pos.getX()].size() > 1) && (pos.getY() < (newVec[pos.getX()].size()-1)))
            {
                LOG(FILE_LOG(logDEBUG2) << "step across";);
                LOG(FILE_LOG(logDEBUG2) << "marked : " << marked;);
                marked = newVec[pos.getX()][pos.getY()+1];
                LOG(FILE_LOG(logDEBUG2) << "new marked : " << marked;);
            }
            else
            {
                if(pos.getX() != 0)
                {
                    LOG(FILE_LOG(logDEBUG2) << "step up";);
                    LOG(FILE_LOG(logDEBUG2) << "marked : " <<  marked;);
                    marked = newVec[pos.getX()-1][0];
                    LOG(FILE_LOG(logDEBUG2) << "new marked : " << marked;);
                }
                else
                {
                    LOG(FILE_LOG(logDEBUG2) << "reached top";);
                    LOG(FILE_LOG(logDEBUG2) << "marked : " <<  marked;);
                    /// if pX is zero find if it exists as a second position in the pairings
                    Pair val;
                    bool top = searchSecondPosition(marked, editVec, val);

                    /// if it does, make the X of that pair marked
                    /// if it does not end the algorithm
                    if(top)
                    {
                        LOG(FILE_LOG(logDEBUG2) << "found else where";);
                        marked = val.getX();
                        LOG(FILE_LOG(logDEBUG2) << marked;);
                    }
                    else
                    {
                        LOG(FILE_LOG(logDEBUG2) << "not found = end";);
                        ended = true;
                    }
                }
            }

            //ended = true;
        }

        display2DIntVector(newVec);
    }

    LOG(FILE_LOG(logDEBUG) << "interpretPOP() --> end";);
}

void Interpret::orderTies(vector<vector<int> > &vec, vector<int> &ordered)
{
    vector<vector<int> >::iterator iter;
    vector<int>::iterator xiter;

    for(iter = vec.begin(); iter != vec.end(); iter++)
    {
        if((*iter).size() > 1)
        {
            xiter = (*iter).begin();
            while((*iter).size() >= 1)
            {
                //int val = (*xiter);
                int place =  rand()%11;
                if(place > 4)
                {
                    ordered.push_back((*xiter));
                    (*iter).erase(xiter);
                    xiter = (*iter).begin();
                }
                else
                {
                    xiter++;
                    if(xiter == (*iter).end())
                    {
                        xiter = (*iter).begin();
                    }
                }
            }
        }
        else
        {
            ordered.push_back((*iter)[0]);
        }
    }
}

void Interpret::placePair(Pair &used, vector<vector<int> > &vec)
{
    int pos = -1;
    Pair pos2;

    /// find position of mark
    bool found = search2DVector(used.getX(), pos, vec);
    bool foundP2 = search2DVector(used.getY(), pos2, vec);

    if(found)
    {
        if(pos == vec.size() -1)
        {
            /// place at end
            vector<int> working;
            working.push_back(used.getY());
            vec.push_back(working);
        }
        else
        {
            /// add to existing vector
            vec[pos+1].push_back(used.getY());
        }
    }
    else
    {
        /// mark not found, thus insert at beginning
        vector<int> working;
        working.push_back(used.getX());
        vector<vector<int> >::iterator iter;
        iter = vec.begin();
        vec.insert(iter, working);
    }

    /// should make this a search function and not a bunch of loops, but I think this may be more efficient
    if(pos == pos2.getX())
    {
        /// delete pos2
        vector<vector<int> >::iterator out_iter;
        out_iter = vec.begin();
        vector<int>::iterator inner_iter;

        /// position outer iterator
        for(int i = 0; i < pos2.getX(); i++)
        {
            out_iter++;
        }

        /// position inner iterator
        inner_iter = (*out_iter).begin();
        for(int i = 0; i < pos2.getY()-1; i++)
        {
            inner_iter++;
        }

        /// erase
        (*out_iter).erase(inner_iter);
    }
}

bool Interpret::fulfilledCheck(Pair &used, vector<vector<int> > &vec)
{
    LOG(FILE_LOG(logDEBUG) << "fulfilledCheck() --> begin";);
    bool fulfilled = false;
    int firstPos = -1;
    int secondPos = -1;

    bool foundX = search2DVector(used.getX(), firstPos, vec);
    bool foundY = search2DVector(used.getY(), secondPos, vec);
    /// if used _Y is found we continue
    if(foundY && foundX)
    {
        if(firstPos < secondPos)
        {
            fulfilled = true;
        }
    }

    LOG(FILE_LOG(logDEBUG) << "fulfilledCheck() --> end";);
    return fulfilled;
}

bool Interpret::search2DVector(int mark, Pair &pos, vector<vector<int> > &vec)
{
    bool found = false;
    int outer_count = 0;
    int inner_count = 0;

    vector<vector<int> >::iterator iter;
    vector<int>::iterator xiter;

    for(iter = vec.begin(); iter != vec.end(); iter++)
    {
        for(xiter = (*iter).begin(); xiter != (*iter).end(); xiter++)
        {
            if((*xiter) == mark)
            {
                found = true;
                pos = Pair(outer_count, inner_count);
                break;
            }
            inner_count++;
        }

        if(found)
        {
            break;
        }

        outer_count++;
    }

    return found;
}

bool Interpret::search2DVector(int mark, int &pos, vector<vector<int> > &vec)
{
    Pair position;
    bool found = search2DVector(mark, position, vec);
    if(found)
    {
        pos = position.getX();
    }

    return found;
}

bool Interpret::searchFirstPosition(int marked, vector<Pair> &vec, Pair &ans)
{
    vector<Pair>::iterator iter;
    iter = vec.begin();
    bool found = false;

    while((iter != vec.end())&&(found == false))
    {
        if((*iter).getX() == marked)
        {
            found = true;
            ans = (*iter);
        }
        iter++;
    }

    return found;
}

bool Interpret::searchSecondPosition(int marked, vector<Pair> &vec, Pair &ans)
{
    vector<Pair>::iterator iter;
    iter = vec.begin();
    bool found = false;

    while((iter != vec.end())&&(found == false))
    {
        if((*iter).getY() == marked)
        {
            found = true;
            ans = (*iter);
        }
        iter++;
    }

    return found;
}

bool Interpret::deletePair(Pair item, vector<Pair> &vec)
{
    bool deleted = false;
    vector<Pair>::iterator iter;
    iter = vec.begin();

    while((iter != vec.end())&&(deleted == false))
    {
        if((*iter) == item)
        {
            deleted = true;
            vec.erase(iter);
        }
        iter++;
    }

    return deleted;
}

void Interpret::cullVector(vector<Pair> &vec)
{
    LOG(FILE_LOG(logDEBUG) << "cullVector() --> begin";);

    vector<Pair>::iterator iter;
    iter = vec.begin();
    while(iter != vec.end())
    {
        //FILE_LOG(logDEBUG1) << (*iter).print();
        if((*iter).getX() == 0)
        {
            //FILE_LOG(logDEBUG1) << "zero - die";
            vec.erase(iter);
            continue;
        }

        if((*iter).getY() == 1)
        {
            //FILE_LOG(logDEBUG1) << "one - die";
            vec.erase(iter);
            continue;
        }

        iter++;
    }

    LOG(FILE_LOG(logDEBUG) << "cullVector() --> end";)
}

void Interpret::displayPairVector(vector<Pair> &vec)
{
    for(unsigned int x = 0; x < vec.size(); x++)
    {
        LOG(FILE_LOG(logDEBUG) << vec[x].print(););
        cout << vec[x].print() << endl;
    }
}

void Interpret::display2DIntVector(vector<vector<int> > &vec)
{
    vector<vector<int> >::iterator iter;
    vector<int>::iterator xiter;

    for(iter = vec.begin(); iter != vec.end(); iter++)
    {
        string line = "";
        Common comm;
        for(xiter = (*iter).begin(); xiter != (*iter).end(); xiter++)
        {
            line.append(comm.intToString((*xiter)));
            line.append(" ");
        }

        LOG(FILE_LOG(logDEBUG) << line;);
    }
}

void Interpret::displayIntVector(vector<int> &vec)
{
    vector<int>::iterator iter;

    for(iter = vec.begin(); iter != vec.end(); iter++)
    {
        LOG(FILE_LOG(logDEBUG) << (*iter););
    }
}


void Interpret::populateVector(vector<Pair> &vec)
{
    vec.push_back(Pair( 2 , 1 ));
    vec.push_back(Pair( 0 , 2 ));
    vec.push_back(Pair( 2 , 1 ));
    vec.push_back(Pair( 3 , 1 ));
    vec.push_back(Pair( 0 , 3 ));
    vec.push_back(Pair( 3 , 1 ));
    vec.push_back(Pair( 0 , 3 ));
    vec.push_back(Pair( 4 , 3 ));
    vec.push_back(Pair( 0 , 4 ));
    vec.push_back(Pair( 4 , 1 ));
    vec.push_back(Pair( 0 , 4 ));
    vec.push_back(Pair( 4 , 2 ));
    vec.push_back(Pair( 5 , 1 ));
    vec.push_back(Pair( 0 , 5 ));
    vec.push_back(Pair( 5 , 1 ));
    vec.push_back(Pair( 0 , 5 ));
    vec.push_back(Pair( 6 , 5 ));
    vec.push_back(Pair( 0 , 6 ));
    vec.push_back(Pair( 6 , 1 ));
    vec.push_back(Pair( 7 , 1 ));
    vec.push_back(Pair( 0 , 7 ));
    vec.push_back(Pair( 7 , 1 ));
    vec.push_back(Pair( 0 , 7 ));
    vec.push_back(Pair( 6 , 7 ));
    vec.push_back(Pair( 4 , 6 ));
    vec.push_back(Pair( 3 , 6 ));
    vec.push_back(Pair( 6 , 2 ));
    vec.push_back(Pair( 5 , 2 ));
    vec.push_back(Pair( 7 , 2 ));
}

void Interpret::populateVector2(vector<Pair> &vec)
{
    vec.push_back(Pair( 6 , 2 ));
    vec.push_back(Pair( 5 , 2 ));
    vec.push_back(Pair( 7 , 2 ));
    vec.push_back(Pair( 4 , 3 ));
    vec.push_back(Pair( 3 , 6 ));
    vec.push_back(Pair( 6 , 5 ));
    vec.push_back(Pair( 4 , 2 ));
    vec.push_back(Pair( 6 , 7 ));
    vec.push_back(Pair( 4 , 6 ));
}
