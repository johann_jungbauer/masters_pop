 #include "POP_Algorithm.h"

 POP_Algorithm::POP_Algorithm()
 {

 }

/********************************************//**
 * \brief The method that runs the main loop of the POP algorithm
 *
 * \param &initial_state const std::vector<Predicate*>
 * \param initial_var_num const int
 * \param &goal_state const std::vector<Predicate*>
 * \param goal_var_num const int
 * \param operators std::vector<Operator *>
 * \param plan Plan&
 * \return bool
 *
 ***********************************************/
bool POP_Algorithm::run_Algorithm(const std::vector<Predicate *> &initial_state, const int initial_var_num , const std::vector<Predicate *> &goal_state, const int goal_var_num, std::vector<Operator *> operators, Plan &plan)
{
    // TODO (clever#1#) Create variable count methods for run_Algorithm.

    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::run_algorithm() -> begin";);

    bool plan_found = false;
    bool operator_found = false;
    int count = 0;

    make_Minimal_Plan(initial_state, initial_var_num, goal_state, goal_var_num);


    //while ((count < 25) && (!solution(plan_found)))
    //while(count < 25)
    while (!solution(plan_found))
    {
        // TODO (efficiency#1#) Might gleen some speed by making these variables part of method scope & not created within loop.
        int step_index =0, precon_index =0;
        Link used_link = Link(0,0,0);

        LOG(FILE_LOG(logDEBUG1) << "\n\n-------\t\tIteration: " << count;);

        /// if false return no subgoal that doesn't link to a failed plan could be found
        /// this should trigger a parent rollback
        if(select_Subgoal(step_index,precon_index) == false)
        {
            /// leaveraging the next if statement to do the rollback work
            LOG(FILE_LOG(logDEBUG1) << "All subgoals linked to failed children - trigger parent rollback.";);
            operator_found = false;
        }
        else
        {
            /// work as per usual
            operator_found = choose_Operator(operators, step_index, precon_index, used_link);
        }

        if(operator_found)
        {
            /// continue as usual
            LOG(FILE_LOG(logDEBUG1) << "\n\t\tOperator found";);
            LOG(FILE_LOG(logDEBUG1) << "used link = " << used_link.print(););

            /// check if there is an inconsistency. A true return denotes a problem
            if(resolve_Threats())
            {
                /// plan is inconsistent
                LOG(FILE_LOG(logDEBUG1) << "Threats could not be resolved.";);

                /// checking if the last change that was done was a step addition or an internal linking.
                if(_step_addition)
                {
                    LOG(FILE_LOG(logDEBUG1) << "break after step addition - rollback to parent.";);
                    parent_Rollback();
                }
                else
                {
                    LOG(FILE_LOG(logDEBUG1) << "break after internal change - rollback to previous plan.";);

                    internal_Rollback(used_link);
                }
            }
            else
            {
                /// successful iteration thus store plan
                LOG(FILE_LOG(logDEBUG1) << "Successful plan stage...";);
                _previous_plan = _current_plan;
            }
        }
        else
        {
            /// operator not found - this plan is a failure rollback to parent or no plan is possible
            LOG(FILE_LOG(logDEBUG1) << "Operator NOT found!!!";);

            /// If we are at node_ID 0 and want to rollback then no plan is possible.
            /// Thus break out of loop.
            if(_current_node_index == 0)
            {
                LOG(FILE_LOG(logDEBUG1) << "Rolling back to parent of plan 0. Total plan failure.";);
                break;
            }
            else
            {
                parent_Rollback();
            }
        }

        LOG(FILE_LOG(logDEBUG1) << "\n---------------------------\n";);
        LOG(FILE_LOG(logDEBUG) << _current_plan.print(););
        LOG(FILE_LOG(logDEBUG) << print_Tree(true););
        count++;
    }

    if(plan_found)
    {
        plan = _current_plan;
    }

    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::run_algorithm() -> end";);

    return plan_found;
}

/* Used to roll the plan back to a previously consistent internal plan.
    This is triggered when in interval plan change, like linking an effect to a precon, causes an inconsistency.
-----------------------------------
Parameters - Link - The link that caused the inconsistency.
Returns - none
*/
void POP_Algorithm::internal_Rollback(Link &used_link)
{
    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::internal_Rollback() -> begin";);

    /// store link that caused failure
    _failed_links.push_back(used_link);
    /// roll back to previous plan state
    LOG(FILE_LOG(logDEBUG1) << "Roll back to previous plan.";);
    /// this lowers the priority of the step
    _previous_plan._steps[used_link.get_Precon_Step_Index()].mod_Punishment(2);
    //_previous_plan.sort_Precon_Tracker();
    _current_plan = _previous_plan;

    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::internal_Rollback() -> end";);
}

/* Used to roll the plan back to a parent node.
    Triggered if a step addition causes an inconsistency or if
    no operator can be found to fulfill the current precondition.
-----------------------------------
Parameters -
Returns - none
*/
void POP_Algorithm::parent_Rollback()
{
    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::parent_Rollback() -> begin";);

    /// store the current plan into its node.
    record_Current_Plan();
    /// set the current as failed
    _tree[_current_node_index]._failed_path = true;
    /// get the parent node index
    int parent = _tree[_current_node_index]._parent_ID;
    /// set current & previous plan to parent plan. Makes sure that an internal rollback doesn't use the failed plan.
    _current_plan = _tree[parent]._plan;
    _previous_plan = _current_plan;

    _current_node_index = parent;
    _failed_links = _tree[parent]._failed_links;

    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::parent_Rollback() -> end";);
}

/* Used to make the base plan of the search tree
-----------------------------------
Parameters -
Returns - none
*/
void POP_Algorithm::make_Minimal_Plan(const std::vector<Predicate *> &initial_state, const int &initial_var_num, const std::vector<Predicate *> &goal_state, const int &goal_var_num)
{
    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::make_Minimal_Plan() -> begin";);

    Mediator_Algorithm med;

    Op_Start start = Op_Start(initial_state, initial_var_num);
    Op_Finish finish = Op_Finish(goal_state, goal_var_num);

    _current_plan.add_Step(start);
    _current_plan.add_Step(finish);

    _step_addition = true;

    _previous_plan = _current_plan;

    _current_node_index = med.add_Plan_To_Tree(_current_plan, _tree, -1, _failed_links, -1, -1, start);

    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::make_Minimal_Plan() -> end";);
}

/* Selects the first unfulfilled precondition in the precon tracker of the current plan
-----------------------------------
Parameters -
Returns -
*/
bool POP_Algorithm::select_Subgoal(int &step_index, int &precon_index)
{
    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::select_Subgoal() -> begin";);

    bool subgoal_found = false;
    bool child_match = false;
    int precon_count = 0;
    int child_count = 0;

    _current_plan.sort_Precon_Tracker();

    LOG(FILE_LOG(logDEBUG1) << _current_plan.print_Precon_Tracker(););

    while((precon_count < _current_plan._precon_tracker.size()) && (subgoal_found == false))
    {
        /// use the values at position precon_count
        step_index = _current_plan._precon_tracker[precon_count].getX();
        precon_index = _current_plan._precon_tracker[precon_count].getY();

        /// if values cannot be found in failed child plans - valid subgoal is found
        /// ie find first subgoal not linked to a failed child.
        child_count = 0;
        child_match = false;
        while((child_count < _tree[_current_node_index]._children.size()) && (child_match == false))
        {
            int child_node_index = _tree[_current_node_index]._children[child_count];

            if((_tree[child_node_index]._failed_path == true) &&
                (_tree[child_node_index]._seed_step_index == step_index) &&
                (_tree[child_node_index]._seed_precon_index == precon_index))
            {
                child_match = true;
            }
            child_count++;
        }

        if(child_match == false)
        {
            subgoal_found = true;
        }

        precon_count++;
    }


    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::select_Subgoal() -> end";);

    return subgoal_found;
}

/* Used to add a plan and its relevant supporting info to the search tree
-----------------------------------
Parameters -
Returns -
*/
std::string POP_Algorithm::print_Tree(bool full_info)
{
    Common common;
    std::string out = "\n--SEARCH TREE--";
    out.append("\nsize : ");
    out.append(common.intToString(_tree.size()));

    for(unsigned int i = 0; i < _tree.size(); i++)
    {
        out.append("\n\nPlan #: ");
        out.append(common.intToString(_tree[i]._node_ID));
        out.append("\nParent: ");
        out.append(common.intToString(_tree[i]._parent_ID));
        out.append("\nFailed: ");
        out.append(common.boolToString(_tree[i]._failed_path));
        out.append("\nChildren : ");
        for(unsigned int x = 0; x < _tree[i]._children.size(); x++)
        {
            out.append(common.intToString(_tree[i]._children[x]));
            out.append(", ");
        }
    }

    if(full_info)
    {
        /// add a full plan print out
    }

    return out;
}

/* Determines whether the current plan is a solution by counting the number of
    remaining preconditions. It is a solution if no preconditions remain.
-----------------------------------
Parameters - bool - reference to the plan_found variable
Returns - bool - whether the current plan is a solution
*/
bool POP_Algorithm::solution(bool &plan_found)
{
    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::solution() -> begin";);

    bool solution = false;

    if(_current_plan._precon_tracker.size() == 0)
    {
        solution = true;
        plan_found = true;
    }

    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::solution() -> end";);

    return solution;
}


/* Used to choose an operator to fulfill the selected precondition
-----------------------------------
Parameters -
Returns - bool - whether an operator was found to fulfill the selected precon
*/
bool POP_Algorithm::choose_Operator(std::vector<Operator *> operators, int step_index, int precon_index, Link &used_link)
{
    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::choose_Operator() -> begin";);

    Mediator_Variable medVar;
    Mediator_Predicate medPred;
    Mediator_Operator medOp;

    Predicate* c_pred = _current_plan._steps[step_index].get_Precon(precon_index);

    co_info(step_index, precon_index, c_pred);

    /// choose an Operator based on the precon predicate type = Sadd
    /// if the precon predicate value is defined either find an existing operator with an effect of that value or set an
    ///     undefined value to the required value.
    /// if the precon predicate is undefined only use existing defined effects to fulfill the precon

    Predicate_Type precon_type = c_pred->get_Predicate_Type();
    int defined_var_count = 0;
    bool defined = c_pred->get_Var_Defined(defined_var_count);
    bool effect_found = false;

    if(defined)
    {
        LOG(FILE_LOG(logDEBUG1) << " defined = true";);

        /// first search steps for existing operator that fulfills precondition
        effect_found = choose_Op_Steps_Defined_Precon(c_pred, precon_type, step_index, precon_index, used_link);

        /// IF an effect was NOT found in steps, dig through the operators
        if(effect_found == false)
        {
            /// this is where new branches in the search tree are created
            effect_found = choose_Op_Ops_Defined_Precon(c_pred, precon_type, step_index, precon_index, used_link);
        }
    }
    else
    {
        LOG(FILE_LOG(logDEBUG1) << "defined = false";);
        unsigned int loop = 0;
        while((effect_found != true) && (loop < _current_plan._steps.size()))
        {
            /// if not in failed list do
            if(!(search_Failed_Links(Link(_current_plan._steps[loop].get_Step_Index(),step_index,precon_index)))
               && (loop != step_index))
            {
                /// do the step[loop] effects match the required type
                /// I do not check the remove list for precon fulfillment
                int loop2 = 0;
                while((effect_found != true) && (loop2 < _current_plan._steps[loop].get_Addition_Array_Size()))
                {
                    if((precon_type == _current_plan._steps[loop].get_Addition(loop2)->get_Predicate_Type()) && (_current_plan._steps[loop].get_Addition(loop2)->get_Var_Defined() == true))
                    {
                        LOG(FILE_LOG(logDEBUG2) << "steps predicate type match";);
                        /// setting undefined variable values
                        if(defined_var_count == 0)
                        {
                            /// then totally over write
                            LOG(FILE_LOG(logDEBUG3) << "no partials, total over write";);
                            *c_pred= *_current_plan._steps[loop].get_Addition(loop2);

                            used_link = Link(_current_plan._steps[loop].get_Step_Index(),step_index,precon_index);
                            _current_plan.add_Link(used_link);
                            _current_plan.add_Ordering(Order(_current_plan._steps[loop].get_Step_Index(), step_index));
                            effect_found = true;
                            _step_addition = false;
                        }
                        else
                        {
                            /// check for matches
                            LOG(FILE_LOG(logDEBUG3) << "partials found";);
                            int i = 0;
                            bool match = false;

                            //LOG(FILE_LOG(logDEBUG3) << loop << " - "<<_current_plan._steps[loop].print_Operator() << " - " <<  loop2 << " - " << medPred.printPredType(_current_plan._steps[loop].get_Addition(loop2)->get_Predicate_Type()););
                            LOG(FILE_LOG(logDEBUG3) << loop << " - "<< medOp.printOpType(_current_plan._steps[loop].get_Operator_Type()) << " - " <<  loop2 << " - " << medPred.printPredType(_current_plan._steps[loop].get_Addition(loop2)->get_Predicate_Type()););

                            /// concider each variable individually
                            while((i < _current_plan._steps[loop].get_Addition(loop2)->get_Array_Size()) && (match != true))
                            {
                                //bool curr_var_defined = _current_plan._steps[loop].get_Effect(loop2)->get_Variable(i)->get_Defined();
                                /// c_pred controls this interaction. If c_pred var equals a var in the currently considered effect we have found a match
                                bool c_pred_var_defined = c_pred->get_Variable(i)->get_Defined();

                                LOG(FILE_LOG(logDEBUG3) << "c_pred_var_defined" << c_pred_var_defined;);

                                if(c_pred_var_defined)
                                {
                                    // Logging in case of error...
                                    // not sure if this needed.
                                    switch(c_pred->get_Variable(i)->get_Type())
                                    {
                                        case NULL_VAR:
                                        {LOG(FILE_LOG(logDEBUG3) << "NULL_VAR check";);}
                                        break;

                                        default:
                                            LOG(FILE_LOG(logDEBUG3) << "unknown var type found!!";);
                                        break;
                                    }

                                    match = medVar.varEquality(_current_plan._steps[loop].get_Addition(loop2)->get_Variable(i), c_pred->get_Variable(i));
                                }

                                i++;
                            }

                            /// if a match is found
                            if(match == true)
                            {
                                *c_pred= *_current_plan._steps[loop].get_Addition(loop2);
                                used_link = Link(_current_plan._steps[loop].get_Step_Index(),step_index,precon_index);
                                _current_plan.add_Link(used_link);
                                _current_plan.add_Ordering(Order(_current_plan._steps[loop].get_Step_Index(), step_index));
                                effect_found = true;
                                _step_addition = false;
                            }
                        }
                    }

                    /// increment loop2
                    loop2++;
                }
            }

            /// increment loop
            loop++;
        }

        if(effect_found == false)
        {
            LOG(FILE_LOG(logDEBUG1) << "ERROR!! --> no usable effect found at all. Tree will handle it";);
        }
    }
    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::choose_Operator() -> end";);

    return effect_found;
}

/* Used to choose an operator from the existing operators in the plan's list of steps.
    A successful link will result in _step_addition to be set to false.
    This is to track that an internal change has happened, thus a rollback does not need to use a parent node.
-----------------------------------
Parameters -
Returns - bool - whether an existing operator was found to fulfill the selected precondition
*/
bool POP_Algorithm::choose_Op_Steps_Defined_Precon(Predicate* c_pred, const Predicate_Type precon_type, const int &step_index, const int &precon_index, Link &used_link)
{
    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::choose_Op_Steps_Defined_Precon() -> begin";);

    Mediator_Variable medVar;
    Mediator_Predicate medPred;
    Mediator_Operator medOp;

    bool effect_found = false;

    unsigned int loop = 0;
    while((effect_found != true) && (loop < _current_plan._steps.size()))
    {
        /// if not in failed list do
        if(!(search_Failed_Links(Link(_current_plan._steps[loop].get_Step_Index(),step_index,precon_index)))
           && (loop != step_index))
        {
            /// do the step[loop] effects match the required type?
            int loop2 = 0;
            while((effect_found != true) && (loop2 < _current_plan._steps[loop].get_Addition_Array_Size()))
            {
                Predicate_Type current_type = _current_plan._steps[loop].get_Addition(loop2)->get_Predicate_Type();
                //LOG(FILE_LOG(logDEBUG1) << loop << " - "<<_current_plan._steps[loop].print_Operator() << " - " <<  loop2 << " - " << medPred.printPredType(_current_plan._steps[loop].get_Addition(loop2)->get_Predicate_Type()););
                LOG(FILE_LOG(logDEBUG1) << loop << " - "<< medOp.printOpType(_current_plan._steps[loop].get_Operator_Type()) << " - " <<  loop2 << " - " << medPred.printPredType(_current_plan._steps[loop].get_Addition(loop2)->get_Predicate_Type()););

                if(current_type != NULL_PRED)
                {
                    /// is the effect in Steps[loop][loop2] fully defined?
                    int current_defined_count = 0;
                    bool current_defined = _current_plan._steps[loop].get_Addition(loop2)->get_Var_Defined(current_defined_count);

                    /// check if the predicate types match. Are we looking at a predicate that's the same as what is needed?
                    if(precon_type == current_type)
                    {
                        if(current_defined == false)
                        {
                            LOG(FILE_LOG(logDEBUG1) << "predicate type match, undefined effect vars";);
                            /// do this if there is an undefined match
                            /// ie change the Steps() effect predicate to match the required precondition
                            // Can't do a direct equality in case the predicate has more than 1 variable.
                            // ie I don't want to over write already defined variables
                            //*_current_plan._steps[loop].get_Effect(loop2) = *c_pred;

                            /// checking for partial defines
                            if(current_defined_count == 0)
                            {
                                LOG(FILE_LOG(logDEBUG1) << "no partials";);
                                /// no partials thus can fully over write
                                *_current_plan._steps[loop].get_Addition(loop2) = *c_pred;
                                used_link = Link(_current_plan._steps[loop].get_Step_Index(),step_index,precon_index);
                                _current_plan.add_Link(used_link);
                                _current_plan.add_Ordering(Order(_current_plan._steps[loop].get_Step_Index(), step_index));
                                effect_found = true;
                                _step_addition = false;
                            }
                            else
                            {
                                // check if the defined values are equal
                                // if there are a pair of defined variables that are not equal end search
                                LOG(FILE_LOG(logDEBUG1) << "partials";);
                                int i = 0;
                                bool mismatch = false;

                                /// concider each variable individually
                                while((i < _current_plan._steps[loop].get_Addition(loop2)->get_Array_Size()) && (mismatch != true))
                                {
                                    bool curr_var_defined = _current_plan._steps[loop].get_Addition(loop2)->get_Variable(i)->get_Defined();

                                    if(curr_var_defined)
                                    {
                                        mismatch = !(medVar.varEquality(_current_plan._steps[loop].get_Addition(loop2)->get_Variable(i), c_pred->get_Variable(i)));
                                    }

                                    i++;
                                }

                                /// if there are no mismatchs at all
                                if(mismatch == false)
                                {
                                    *_current_plan._steps[loop].get_Addition(loop2) = *c_pred;
                                    used_link = Link(_current_plan._steps[loop].get_Step_Index(),step_index,precon_index);
                                    _current_plan.add_Link(used_link);
                                    _current_plan.add_Ordering(Order(_current_plan._steps[loop].get_Step_Index(), step_index));
                                    effect_found = true;
                                    _step_addition = false;
                                }

                            }
                        }
                        else /// if the steps effect is fully defined...
                        {
                            /// I want to exclude START from linking directly to FINISH
                            /// ie step 0 linking to step 1 in this implementation
                            if(!((loop == 0) && (step_index == 1)))
                            {
                                /// do this if there is a "whole" match
                                /// ie the effect totally matches the precon
                                if((*c_pred == *_current_plan._steps[loop].get_Addition(loop2)))
                                {
                                    LOG(FILE_LOG(logDEBUG1) << "whole match, predicate type and variables";);
                                    used_link = Link(_current_plan._steps[loop].get_Step_Index(),step_index,precon_index);
                                    _current_plan.add_Link(used_link);
                                    _current_plan.add_Ordering(Order(_current_plan._steps[loop].get_Step_Index(), step_index));
                                    effect_found = true;
                                    _step_addition = false;
                                }
                            }
                        }

                    }
                }

                /// increment loop2
                loop2++;
            }
        }

        /// increment loop
        loop++;
    }

    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::choose_Op_Steps_Defined_Precon() -> end";);

    return effect_found;
}

/********************************************//**
 * \brief Used to choose an operator from the list of possible operators.
    Finding an operator here will lead to the creation of a new node in the search tree.
    The _previous_index will point to the parent node. The _current_index will point to the new node.
    _previous_plan will remain unchanged. _current_plan will have the properties of the new plan.
    Returns true if an operator was found to fulfill the selected precondition.
 *
 * \param c_pred Predicate*
 * \param precon_type const Predicate_Type
 * \param step_index const int&
 * \param precon_index const int&
 * \param used_link Link&
 * \return bool
 *
 ***********************************************/
bool POP_Algorithm::choose_Op_Ops_Defined_Precon(Predicate* c_pred, const Predicate_Type precon_type, const int &step_index, const int &precon_index, Link &used_link)
{
    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::choose_Op_Ops_Defined_Precon() -> begin";);

    Mediator_Algorithm medAlgor;
    bool effect_found = false;

    LOG(FILE_LOG(logDEBUG1) << "operators effect search";);
    /// case statement with predicates and corresponding operator types that contain them as effects
    /// leaving Op_Start and Op_Finish out of consideration here since they were checked in the steps check

    Operator_Type op_type;
    if(medAlgor.findOperatorFirst(precon_type, op_type))
    {
        effect_found = true;

        /// Record plan, without addition, into its tree node.
        record_Current_Plan();

        /// Add new Operator to plan.
        medAlgor.addOperator(op_type, c_pred, _current_plan, used_link, step_index, precon_index, _current_node_index, _tree, _failed_links);

        /// define that a step addition has occured
        _step_addition = true;
    }
    else
    {
        /// if you reach here FAIL!!
        LOG(FILE_LOG(logDEBUG1) << "super FAIL match - no operator is available to fulfill selected precondition";);
        effect_found = false;
    }

    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::choose_Op_Ops_Defined_Precon() -> end";);

    return effect_found;
}

/* Used to record the plan the algorithm is currently working on in its tree node.
-----------------------------------
Parameters -
Returns -
*/
void POP_Algorithm::record_Current_Plan()
{
    _tree[_current_node_index]._plan = _current_plan;
    _tree[_current_node_index]._failed_links = _failed_links;
}

/* Used to search the list of failed links for the link provided.
-----------------------------------
Parameters -
Returns -
*/
bool POP_Algorithm::search_Failed_Links(Link element)
{
    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::search_Failed_Links() -> begin";);
    LOG(FILE_LOG(logDEBUG) << element.print(););

    bool found = false;

    if(std::find(_failed_links.begin(), _failed_links.end(), element) != _failed_links.end())
    {
        found = true;
    }

    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::search_Failed_Links() -> end";);

    return found;
}

/********************************************//**
 * \brief Used to log S_need and c info.
 *
 * \param step_index const int&
 * \param precon_index const int&
 * \param c_pred Predicate*
 * \return void
 *
 ***********************************************/
void POP_Algorithm::co_info(const int &step_index, const int &precon_index, Predicate* c_pred)
{
    Mediator_Variable medVar;
    Mediator_Predicate medPred;

    LOG(FILE_LOG(logDEBUG1) << "[S_need : c ] = [ " << step_index << " : " << precon_index << " ]";);

    LOG(FILE_LOG(logDEBUG1) << medPred.printPredType(c_pred->get_Predicate_Type()) << " - " << c_pred->get_Array_Size(););

    for(int i =0; i < c_pred->get_Array_Size(); i++)
    {
        LOG(FILE_LOG(logDEBUG1) << "   "<< medVar.printVar(c_pred->get_Variable(i)););
    }
}

/********************************************//**
 * \brief Used to check the plan for possible predicate threats and then try resolve the threats.
            A value of true is returned if a threat was found and could not be dealt with.
 *
 * \return bool
 *
 ***********************************************/
bool POP_Algorithm::resolve_Threats()
{
    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::resolve_Threats() -> begin";);

    Mediator_Predicate medPred;

    bool threat = false;

    /// initial consistency check
    if(consistent())
    {
        /// check for threats in STEPS
        unsigned int i = 0;
        while((threat == false) && (i < _current_plan._steps.size()))
        {
            /// Loop through the remove list of the operator if the remove list is valid
            /// checking if the first entry is a valid predicate
            if(_current_plan._steps[i].get_Remove(0)->get_Predicate_Type() != NULL_PRED)
            {
                LOG(FILE_LOG(logDEBUG1) << "possible threat";);

                /// Valid remove list. Now loop through it.
                int j = 0;
                while((threat == false) && (j < _current_plan._steps[i].get_Remove_Array_Size()))
                {
                    Predicate* pred_threat = _current_plan._steps[i].get_Remove(j);

                    /// check if threat predicate has a defined value.
                    if(pred_threat->get_Var_Defined())
                    {
                        /// loop through LINKS
                        unsigned int x = 0;
                        while((threat == false) && (x < _current_plan._links.size()))
                        {
                            // TODO (coding#1#) get rid of this string. It gets called & allocated even in release version for no reason.
                            std::string strLink = _current_plan._links[x].print();

                            /// getting the step index of the precon in the Link we are checking
                            int precon_step_index = _current_plan._links[x].get_Precon_Step_Index();
                            int precon_index = _current_plan._links[x].get_Precon_Index();

                            /// getting at predicate we are checking against the threat predicate
                            Predicate *pred_check = _current_plan._steps[precon_step_index].get_Precon(precon_index);

                            // TODO (coding#1#) get rid of this string. It gets called & allocated even in release version for no reason.
                            std::string strCheck = medPred.printPred(pred_check);

                            int int_threat = _current_plan._steps[i].get_Step_Index();
                            int int_check = _current_plan._steps[precon_step_index].get_Step_Index();

                            bool changed = false;

                            /// Searching for existing ordering rules for this operator pairing
                            bool before = _current_plan.find_Order(Order(_current_plan._steps[i].get_Step_Index(), _current_plan._links[x].get_Effect_Step_Index()));
                            bool after = _current_plan.find_Order(Order(precon_step_index, _current_plan._steps[i].get_Step_Index()));
                            //LOG(FILE_LOG(logDEBUG2) << "before = " << before;);
                            //LOG(FILE_LOG(logDEBUG2) << "after = " << after;);

                            /// Checking if 1)the operators we are considering are different, ie not checking if it threatens it self.
                            ///             2) The predicates are equal
                            ///             3+4) That there are no existing ordering rules
                            if((int_threat != int_check) && (*pred_threat == *pred_check) && ((before == false) && (after == false)))
                            {
                                LOG(FILE_LOG(logDEBUG2) << "definite threat -- predicate values are equal";);
                                /// demote unless we are concidering FINISH, then promote
                                if(precon_step_index != 1)
                                {
                                    LOG(FILE_LOG(logDEBUG2) << "demotion";);
                                    Order ord = Order(precon_step_index, _current_plan._steps[i].get_Step_Index());
                                    LOG(FILE_LOG(logDEBUG2) << int_threat << " vs " << int_check << " for link " << strLink << " adding " << ord.print(););
                                    changed = true;
                                    _current_plan.add_Ordering(ord);
                                }
                                else
                                {
                                    LOG(FILE_LOG(logDEBUG2) << "promotion";);
                                    Order ord = Order(_current_plan._steps[i].get_Step_Index(), _current_plan._links[x].get_Effect_Step_Index());
                                    LOG(FILE_LOG(logDEBUG2) << int_threat << " vs " << int_check << " for link " << strLink << " adding " << ord.print(););
                                    changed = true;
                                    _current_plan.add_Ordering(ord);
                                }
                            }

                            /// If an order rule has been added we do a consistency check on the orderings.
                            if(changed)
                            {
                                /// If the check finds an inconsistency we flag a threat and fail the plan
                                if(order_consistent() == false)
                                {
                                    LOG(FILE_LOG(logDEBUG1) << "inconsistent plan";);
                                    threat = true;
                                }
                            }

                            /// increment links list loop variable
                            x++;
                        }//while end x
                    }

                    /// increment remove list loop variable
                    j++;
                }//while end j
            }

            /// increment steps loop variable
            i++;
        }//while end i
    }
    else
    {
        threat = true;
    }

    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::resolve_Threats() -> end";);

    return threat;
}

/* Used to check the plan for contradictions, either variable rule contradictions or ordering contradictions.
-----------------------------------
Parameters -
Returns - bool - A value of true if the plan is consistent;
*/
bool POP_Algorithm::consistent()
{
    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::consistent() -> begin";);

    bool consistent_plan = false;

    /// 2 step check
    /// A: check operator variable rules for contradictions
    ///     - loop through STEPS and call test method
    /// B: check ordering list for contradictions
    ///     - loop through ORDERINGS foreach order pair looking for contradictions

    if(var_consistent() && order_consistent())
    {
        consistent_plan = true;
    }

    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::consistent() -> end";);

    return consistent_plan;
}

/* Used to check the plan for variable rule contradictions.
-----------------------------------
Parameters -
Returns - bool - A value of true if variable rules are consistent;
*/
bool POP_Algorithm::var_consistent()
{
    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::var_consistent() -> begin";);

    bool var_consistent = true;
    //Mediator_Algorithm medAlgor;
    Mediator_Operator medOp;

    /// loop through STEPS and check the variable rules
    for(unsigned int i = 0; i < _current_plan._steps.size(); i++)
    {
        /// if there is a variable rule
        bool rule_bool = false;
        rule_bool = _current_plan._steps[i].get_Var_Rule();

        if(rule_bool == true)
        {
            var_consistent = medOp.checkOperatorRule(&_current_plan._steps[i]);
        }

        if(var_consistent == false)
        {
            /// break out of loop
            break;
        }
    }


    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::var_consistent() -> end";);

    return var_consistent;
}

/* Used to check the plan for ordering contradictions.
-----------------------------------
Parameters -
Returns - bool - A value of true if ordering is consistent;
*/
bool POP_Algorithm::order_consistent()
{
    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::order_consistent() -> begin";);

    bool order_consistent = true;

    for(unsigned int i = 0; i < _current_plan._ordering.size(); i++)
    {
        for(unsigned int j = 0; j < _current_plan._ordering.size(); j++)
        {
            if((_current_plan._ordering[i].get_Preceeding_Step_Index() == _current_plan._ordering[j].get_Following_Step_Index())
               && (_current_plan._ordering[i].get_Following_Step_Index() == _current_plan._ordering[j].get_Preceeding_Step_Index()))
               {
                   LOG(FILE_LOG(logDEBUG1) << "order inconsistency found";);
                   LOG(FILE_LOG(logDEBUG1) << "< " << _current_plan._ordering[i].get_Preceeding_Step_Index() << " , " << _current_plan._ordering[i].get_Following_Step_Index()
                   << " > vs < " << _current_plan._ordering[j].get_Preceeding_Step_Index() << " , " << _current_plan._ordering[j].get_Following_Step_Index() << " >";);

                   order_consistent = false;
                   /// break out of inner loop
                   break;
               }
        }

        if(order_consistent == false)
        {
            /// break out of outer loop
            break;
        }
    }


    LOG(FILE_LOG(logDEBUG) << "POP_Algorithm::order_consistent() -> end";);

    return order_consistent;
}
