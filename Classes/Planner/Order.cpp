#include "Order.h"

Order::Order(int preceeding ,int following):
    _preceeding_Step_Index(preceeding),
    _following_Step_Index(following)
{}

Order::Order():
    _preceeding_Step_Index(-1),
    _following_Step_Index(-1)
{}

int Order::get_Preceeding_Step_Index()
{
    return _preceeding_Step_Index;
}

void Order::set_Preceeding_Step_Index(int index)
{
    _preceeding_Step_Index = index;
}

int Order::get_Following_Step_Index()
{
    return _following_Step_Index;
}

void Order::set_Following_Step_Index(int index)
{
    _following_Step_Index = index;
}

std::string Order::print()
{
    Common comm;
    std::string out = "< ";
    out.append(comm.intToString(_preceeding_Step_Index));
    out.append(" , ");
    out.append(comm.intToString(_following_Step_Index));
    out.append(" >");

    return out;
}

bool Order::operator==(const Order &other) const
{
    bool equal = false;

    if((this->_preceeding_Step_Index == other._preceeding_Step_Index) && (this->_following_Step_Index == other._following_Step_Index))
    {
        equal = true;
    }

    return equal;
}

bool Order::operator!=(const Order &other) const
{
    return !(*this == other);
}
