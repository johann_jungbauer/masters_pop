
# Partial-Order Planner Implementation

This is the partial-order planner implementation I used in my masters work.

The code was initially written in 2012. It was moved here from a backup after the first host location was no longer available.

## Description

This code solves the shopping world planning example found in the 1995 edition of Artificial Intelligence: A Modern Approach.

Start world state, finish state and variables are hard-coded in **Planner::getAgentActionSequence()** 
