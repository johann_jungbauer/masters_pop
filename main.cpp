/// Third iteration of the POP algorithm.
/// Johann Jungbauer
/// 25 July 2012

#include <iostream>

#include "Classes/Common/log.h"
#include "Classes/Planner.h"

using namespace std;

int main()
{
    /// setting up the debug log file, only used if _DEBUG is defined
    /// _DEBUG is #defined in build options
    #ifdef _DEBUG
    FILELog::ReportingLevel() = logDEBUG3;
    FILE* log_fd = fopen( "mylogfile.txt", "w" );
    Output2FILE::Stream() = log_fd;
    #endif

    LOG(FILE_LOG(logDEBUG) << "main() -> starting program";);

    Planner _planner;
    if(_planner.getAgentActionSequence())
    {
        cout << "a viable plan was found. YAY!" << endl;
    }
    else
    {
        cout << "a viable plan was NOT found. NOOOOOO!!!!!" << endl;
    }


    return 0;
}
